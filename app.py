from flask import Flask, render_template, redirect, request, jsonify
from flask.helpers import url_for
from forms.forms import Busqueda_Paciente, Confirmar_cita_form, Login_form, Nueva_cita_form, Registro_form
import random  # generar id cita de manera aleatoria
import sqlite3


app = Flask(__name__)
app.secret_key = '764583nmn24ghk@jhid8fuoislkmfHFGEygh@JNOIu6756YGhjbBF453657thbk@MY7678Ykjnkn'
db = 'db/dbg5.db'


# paciente prueba
usuarios_lista = [
    {
        'nombres': 'Gabo',
        'apellidos': 'Cer',
        'tipo_doc': 'cedula',
        'documento': '123',
        'genero': 'm',
        'email': 'correo@correo.com',
        'password': '123',
        'tipo_usuario': '3',
        'estado_usuario': 'a'
    },
    {
        'nombres': 'Carlos',
        'apellidos': 'Cer',
        'tipo_doc': 'cedula',
        'documento': '1234',
        'genero': 'm',
        'email': 'correo@gmail.com',
        'password': '123',
        'tipo_usuario': '3',
        'estado_usuario': 'a'
    },
    {
        'nombres': 'Lu',
        'apellidos': 'Del',
        'tipo_doc': 'cedula',
        'documento': '9999',
        'genero': 'f',
        'email': 'admin@correo.com',
        'password': '123',
        'tipo_usuario': '1',
        'estado_usuario': 'a'
    }
]

id_usuario_sesion = ''

correos = {
    'correo@correo.com': {
        'id_paciente': '123',
        'password': '123'
    },
    'correo@gmail.com': {
        'id_paciente': '1234',
        'password': '123'
    }
}

citas = [
    {
        'id_paciente': '123',
        'id_cita': '345',
        'estado': 'pendiente',
        'id_medico': '1',
        'especialidad': 'medicina-general',
        'fecha': '2021-10-20',
        'turno': 1,
        'comentarios': '',
        'puntuacion': ''
    },
    {
        'id_paciente': '1234',
        'id_cita': '789',
        'estado': 'finalizada',
        'id_medico': '2',
        'especialidad': 'medicina-general',
        'fecha': '2021-10-09',
        'turno': 2,
        'comentarios': '',
        'puntuacion': ''
    },
    {
        'id_paciente': '123',
        'id_cita': '7890',
        'estado': 'finalizada',
        'id_medico': '2',
        'especialidad': 'medicina-general',
        'fecha': '2021-10-04',
        'turno': 3,
        'comentarios': '',
        'puntuacion': ''
    },
    {
        'id_paciente': '1234',
        'id_cita': '5789',
        'estado': 'pendiente',
        'id_medico': '1',
        'especialidad': 'medicina-general',
        'fecha': '2021-10-22',
        'turno': 2,
        'comentarios': '',
        'puntuacion': ''
    },
    {
        'id_paciente': '123',
        'id_cita': '34512',
        'estado': 'cancelada',
        'id_medico': '1',
        'especialidad': 'medicina-general',
        'fecha': '2021-10-22',
        'turno': '-',
        'comentarios': '',
        'puntuacion': ''
    },
    {
        'id_paciente': '1234',
        'id_cita': '78900',
        'estado': 'cancelada',
        'id_medico': '1',
        'especialidad': 'medicina-general',
        'fecha': '2021-10-20',
        'turno': '-',
        'comentarios': '',
        'puntuacion': ''
    },
    {
        'id_paciente': '1234',
        'id_cita': '3',
        'estado': 'finalizada',
        'id_medico': '342',
        'especialidad': 'odontología',
        'fecha': '2021-10-01',
        'turno': 4,
        'comentarios': '',
        'puntuacion': ''
    },
]

# turnos


@app.route('/')
def index():
    title = 'Inicio'
    return render_template('index.html', title=title)


@app.route('/iniciar-sesion', methods=['GET', 'POST'])
def login():
    title = 'Login'
    login_form = Login_form(request.form)

    if (login_form.validate_on_submit()):
        email = login_form.emailuser.data
        password = login_form.password_login.data
        # conectamos con la base de datos
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute('SELECT idUsuario, nombreUsuario, apellidoUsuario, numDocumento, estado, fk_idRol FROM usuario where correoElectronico = ? and password = ? and estado = 1', [
                           email, password])

            # guardamos el resultado unico en una variable llamado row
            row = cursor.fetchone()
            if row:
                # almacenamos el nombre de las columnas
                columns = [column[0] for column in cursor.description]
                # creamos un diccionario para realizar las busquedas y pasar datos
                usuario = dict(zip(columns, row))
                # print(usuario)
                # rederigimos a los usuarios segun su rol
                if usuario['fk_idRol'] == 3:
                    return redirect(url_for('.pacient', id=usuario['numDocumento']))

                if usuario['fk_idRol'] == 2:
                    return 'Medico'

                if usuario['fk_idRol'] == 1:
                    return redirect(url_for('.admin_inicio', id=usuario['numDocumento']))

            else:
                return 'Usuario o password incorrectos'

    return render_template('/registro-login/login.html',
                           title_login=title,
                           form=login_form)


@app.route('/registro-paciente', methods=['GET', 'POST'])
def registro_paciente():
    title = 'Registro'
    registro_form = Registro_form(request.form)

    if registro_form.validate_on_submit():
        nombres = registro_form.nombres_usuario.data
        apellidos = registro_form.apellidos_usuario.data
        tipo_doc = registro_form.tipo_documento.data
        doc = registro_form.documento_usuario.data
        genero = registro_form.genero_usuario.data
        email = registro_form.email_usuario.data
        password = registro_form.password_usuario.data
        registro_user = True

        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                'SELECT correoElectronico, numDocumento FROM usuario where correoElectronico = ? or numDocumento = ?', [email, doc])
            if cursor.fetchone():
                return 'Correo o Documento ya registrado'
            else:
                cursor.execute('INSERT INTO usuario(nombreUsuario,apellidoUsuario,tipoDocumento,numDocumento,correoElectronico,password,estado,fk_idRol) VALUES(?,?,?,?,?,?,?,?)', [
                               nombres, apellidos, tipo_doc, doc, email, password, 1, 3])
                con.commit()
                return 'Registrando'

    return render_template('/registro-login/registro.html',
                           title=title,
                           form=registro_form)


# Ruta paciente inicio
@app.route('/paciente/<id>', methods=['GET'])
def paciente(id):
    with sqlite3.connect(db) as con:
        cursor = con.cursor()
        cursor.execute(
            'SELECT nombreUsuario, apellidoUsuario, numDocumento FROM usuario where numDocumento = ? and fk_idRol = ?', [id, 3])
        row = cursor.fetchone()
        if row:
            columns = [column[0] for column in cursor.description]
            print(columns)
            usuario = dict(zip(columns, row))
            print(usuario)

    return render_template('/paciente-view/paciente-inicio.html', id=id, nombres=usuario['nombreUsuario'], apellidos=usuario['apellidoUsuario'])


# Ruta paciente cuando selecciona una especialidad para crear una nueva cita
@app.route('/paciente/nueva-cita/<especialidad>/<id>', methods=['GET', 'POST'])
def nueva_cita(especialidad, id):
    # cita_form = Nueva_cita_form(request.form)
    confirmar_form = Confirmar_cita_form(request.form)

    if confirmar_form.validate_on_submit():
        fecha = request.form['confirmar_fecha_cita']
        espec_cita = request.form['confirmar_especialidad_cita']
        medico = request.form['confirmar_medico_cita']
        turno = request.form['confirmar_turno_cita']
        cita_valida = True
        id_cita = ''

        if espec_cita == 'medicina-general':
            id_cita = f'MG{random.randint(0,9999)}'
        elif espec_cita == 'odontologia':
            id_cita = f'OD{random.randint(0,9999)}'
        elif espec_cita == 'oftalmologia':
            id_cita = f'OF{random.randint(0,9999)}'
        c = {
            'id_paciente': id,
            'id_cita': id_cita,
            'estado': 'pendiente',
            'id_medico': medico,
            'especialidad': espec_cita,
            'fecha': fecha,
            'turno': turno,
            'comentarios': '',
            'puntuacion': ''
        }
        citas.append(c)
        return redirect(url_for('.citas_pendientes', id=id))
    else:
        print('NOP')

    return render_template('paciente-view/nuevacita.html',
                           especialidad=especialidad,
                           id=id,
                           #    form=cita_form,
                           conf_form=confirmar_form)

# ruta prueba


@app.route('/mostrarCita/<id_cita>')
def mostrarCita(id_cita):
    cita = []
    for c in citas:
        if c['id_cita'] == id_cita:
            cita.append(c)

    return jsonify(citas)


@app.route('/paciente/validar-cita/<fecha>/<especialidad>/<medico>/<id>', methods=['GET'])
def validar_cita(fecha, especialidad, medico, id):
    cita = []
    contTurnos = 0
    turnos = []
    # validamos si hay citas en esa fecha, con ese medico y en esa especialidad
    for cita in citas:
        if fecha == cita['fecha'] and medico == cita['id_medico'] and especialidad == cita['especialidad'] and cita['estado'] == 'pendiente':
            contTurnos += 1
            turnos.append(cita['turno'])

    if contTurnos >= 20:
        cita = {
            'disponible': False,
            'mensaje': f'No hay cita disponible para el dia "{fecha}"',
            'informacion': {}
        }
    else:
        turno = 1
        for t in turnos:
            if turno == int(t):
                turno += 1
            else:
                turno = turno
                break
        cita = {
            'disponible': True,
            'mensaje': 'Cita disponible',
            'informacion': {
                'fecha': fecha,
                'turno': turno,
                'especialidad': especialidad,
                'medico': medico,
                'id_paciente': id,
                'ciudad': 'Barranquilla'
            }
        }
    return jsonify(cita)


# Ruta paciente para acceder a las citas
@app.route('/paciente/mis-citas/<id>', methods=['GET'])
def mis_citas(id):
    return render_template('paciente-view/miscitas.html', id=id)


# Ruta paciente para ver las citas pendientes
@app.route('/paciente/mis-citas/citas-pendientes/<id>', methods=['GET'])
def citas_pendientes(id):
    citas_usuario = []
    # se busca en el dicc de citas, aquellas citas que tengan en id_paciente igual al id
    # y luego aquellas que tienen el estado pendiente
    for c in citas:
        if c['id_paciente'] == id:
            if c['estado'] == 'pendiente':
                citas_usuario.append(c)
    return render_template('paciente-view/citaspendientes.html', id=id, citas=citas_usuario)


@app.route('/paciente/mis-citas/citas-canceladas-finalizadas/<id>', methods=['GET'])
def citas_canceladas_finalizadas(id):
    citas_usuario = []
    # se busca en el dicc de citas, aquellas citas que tengan en id_paciente igual al id
    # y luego aquellas que tienen el estado en cancelada o finalizada
    for c in citas:
        if c['id_paciente'] == id:
            if c['estado'] == 'finalizada' or c['estado'] == 'cancelada':
                citas_usuario.append(c)
    return render_template('paciente-view/citascanceladasfinalizadas.html', id=id, citas=citas_usuario)


# ruta prueba
@app.route('/pendientes/<id>')
def pendientes(id):
    citas_usuario = []

    for c in citas:
        if c['id_paciente'] == id:
            if c['estado'] == 'pendiente':
                citas_usuario.append(c)
    return jsonify(citas_usuario)


'''
+++++++++++++++++++++++++++ADMIN++++++++++++++++++++++++++++++++++
'''


@app.route('/admin/<id>', methods=['GET'])
def admin_inicio(id):
    with sqlite3.connect(db) as con:
        cursor = con.cursor()
        cursor.execute(
            'SELECT nombreUsuario, apellidoUsuario, numDocumento FROM usuario where numDocumento = ? and fk_idRol = ?', [id, 1])
        row = cursor.fetchone()
        if row:
            columns = [column[0] for column in cursor.description]
            print(columns)
            admin = dict(zip(columns, row))
            print(admin)
    return render_template('admin/admin-inicio.html', id=id, nombres=admin['nombreUsuario'], apellidos=admin['apellidoUsuario'])


@app.route('/admin/<id>/pacientes', methods=['GET', 'POST'])
def admin_pacientes(id):
    busqueda_paciente_form = Busqueda_Paciente(request.form)

    if busqueda_paciente_form.validate_on_submit():
        dato_paciente = request.form['buscar_paciente']
        return redirect(url_for('.pacientes_resultados', dato=dato_paciente))

    return render_template('admin/admin-pacientes.html', id=id, form_paciente=busqueda_paciente_form)


@app.route('/pacientes-resultados/<dato>', methods=['GET'])
def pacientes_resultados(dato):
    mensaje = ''
    encontrados = 0
    pacientes = []
    resultados = {}

    with sqlite3.connect(db) as con:
        cursor = con.cursor()

        cursor.execute('SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.estado, r.nombreRol FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol WHERE u.fk_idRol = 3 AND (LOWER(u.nombreUsuario) = LOWER(?) or LOWER(u.apellidoUsuario)= LOWER(?) or LOWER(u.numDocumento) = LOWER(?) or LOWER(u.correoElectronico) = LOWER(?))', [
                       dato.lower(), dato.lower(), dato.lower(), dato.lower()])

        rows = cursor.fetchall()
        if rows:
            columns = [column[0] for column in cursor.description]
            print(columns)
            for row in rows:
                cont = 0
                encontrados += 1
                paciente = {}
                for r in row:
                    paciente[columns[cont]] = r
                    cont += 1
                pacientes.append(paciente)
            print(pacientes)

            resultados = {
                'mensaje': 'Paciente(s) encontrado(s)' if encontrados > 0 else 'Paciente no encontrado',
                'pacientes_encontrados': encontrados,
                'resultado': pacientes
            }
            return jsonify(resultados)

        else:
            resultados = {
                'mensaje': 'Paciente(s) encontrado(s)' if encontrados > 0 else 'Paciente no encontrado',
                'pacientes_encontrados': encontrados,
                'resultado': pacientes
            }
            return jsonify(resultados)


if __name__ == '__main__':
    app.run(debug=True)
