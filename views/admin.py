
from flask import Blueprint, json, render_template, redirect, request, jsonify, flash, session
from flask.helpers import url_for
from forms.forms import Busqueda_Paciente, Busqueda_Medico, Crear_Medico_Form, Crear_Paciente_Form
import sqlite3
import os
import pandas as pd
import matplotlib.pyplot as plt
import hashlib  # encriptar
from werkzeug.utils import escape


admin = Blueprint('admin', __name__)


admin.secret_key = os.urandom(32)
#Utilizar cuando se suba a la nube
#homeServer = "/home/pmonteroa/mysite/"
homeServer = ""
db = f'{homeServer}db/dbProyecto.db'


@admin.route("/<id>", methods=["GET"])
def init(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        if session['id_usuario'] == id:
            with sqlite3.connect(db) as con:
                cursor = con.cursor()
                cursor.execute(
                    'SELECT nombreUsuario, apellidoUsuario, numDocumento FROM usuario where numDocumento = ? and fk_idRol = ?', [id, 1])
                row = cursor.fetchone()
                if row:
                    columns = [column[0] for column in cursor.description]
                    #print(columns)
                    admin = dict(zip(columns, row))
                    #print(admin)
            return render_template('admin/admin-inicio.html', id=id, nombres=admin['nombreUsuario'], apellidos=admin['apellidoUsuario'])
        else:
            return redirect('/logout')
    else:
        return redirect('/logout')


'''
RUTA ADMIN PACIENTES
'''


@admin.route('/<id>/pacientes', methods=['GET', 'POST'])
def admin_pacientes(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        busqueda_paciente_form = Busqueda_Paciente(request.form)

        if busqueda_paciente_form.validate_on_submit():
            dato_paciente = request.form['buscar_paciente']
            return redirect(url_for('.pacientes_resultados', dato=dato_paciente))

        return render_template('admin/admin-pacientes.html', id=id, form_paciente=busqueda_paciente_form)

    else:
        return redirect('/logout')



'''
RUTA consultar pacientes
Esta ruta devolverá un JSON para que los datos sean mostrados mediante una peticion AJAX
'''

# RUTA DE CONSULTA UNICAMENTE


@admin.route('/pacientes-resultados/<dato>', methods=['GET'])
def pacientes_resultados(dato):
    if 'id_usuario' in session:
        mensaje = ''
        encontrados = 0
        pacientes = []
        resultados = {}

        with sqlite3.connect(db) as con:
            cursor = con.cursor()

            cursor.execute('SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.estado, r.nombreRol, p.genero FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol join Paciente as p on p.idPaciente = u.numDocumento WHERE u.fk_idRol = 3 AND (LOWER(u.nombreUsuario) = LOWER(?) or LOWER(u.apellidoUsuario)= LOWER(?) or LOWER(u.numDocumento) = LOWER(?) or LOWER(u.correoElectronico) = LOWER(?));', [
                dato.lower(), dato.lower(), dato.lower(), dato.lower()])

            rows = cursor.fetchall()
            if rows:
                columns = [column[0] for column in cursor.description]
                # print(columns)
                for row in rows:
                    cont = 0
                    encontrados += 1
                    paciente = {}
                    for r in row:
                        paciente[columns[cont]] = r
                        cont += 1
                    pacientes.append(paciente)
                # print(pacientes)

                resultados = {
                    'mensaje': 'Paciente(s) encontrado(s)',
                    'pacientes_encontrados': encontrados,
                    'resultado': pacientes
                }
                return jsonify(resultados)

            else:
                resultados = {
                    'mensaje': 'Paciente no encontrado',
                    'pacientes_encontrados': encontrados,
                    'resultado': pacientes
                }
                return jsonify(resultados)
    else:
        return redirect('/logout')


'''
Ruta para consultar paciente si existe en la base de datos
En caso que exista, esta ruta servira para mostrar los datos y que se puedan editar
'''


@admin.route('/paciente-consultar/<id_paciente>')
def paciente_consultar(id_paciente):
    if 'id_usuario' in session:
        resp = {}

        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            cursor.execute('''SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.fk_idrol, p.genero
            FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol
            join Paciente as p on p.idPaciente = u.numDocumento
            WHERE u.numDocumento = ? and u.fk_idrol = 3;
            ''', [id_paciente])

            row = cursor.fetchone()

            if row:
                resp = {
                    'respuesta': True,
                    'mensaje': 'Paciente encontrado',
                    'informacion': {
                        'tipo_documento': row['tipoDocumento'],
                        'documento': row['numDocumento'],
                        'nombres': row['nombreUsuario'],
                        'apellidos': row['apellidoUsuario'],
                        'correo': row['correoElectronico'],
                        'genero': row['genero'],
                        'rol': row['fk_idRol']
                    }
                }
            else:
                resp = {
                    'respuesta': False,
                    'mensaje': 'Paciente no encontrado',
                    'informacion': {}
                }

            return jsonify(resp)
    else:
        return redirect('/logout')


'''
Ruta para editar al paciente y actualizarlo
'''
# Ruta para actualizar paciente, no recibe parametros en la url, sino desde el formulario
# no actualiza el documento, no se porque :(


@admin.route('/paciente-editar', methods=['POST'])
def editar_paciente():
    if 'id_usuario' in session:
        tipo_documento = request.form["editar_tipo_documento"]
        documento = request.form["editar_documento"]
        nombres = request.form["editar_nombres"]
        apellidos = request.form["editar_apellidos"]
        genero = request.form["editar_genero"]
        correo = request.form["editar_correo"]
        rol = request.form["editar_rol"]
        resp = {}

        if len(documento) > 0 and len(tipo_documento) > 0 and len(nombres) > 0 and len(apellidos) > 0 and len(genero) > 0 and len(correo) > 0 and len(rol) > 0:
            if(int(documento)):
                with sqlite3.connect(db) as con:
                    cursor = con.cursor()

                    cursor.execute(''' UPDATE usuario
                            SET nombreUsuario = ?, apellidoUsuario = ?, numDocumento = ?, tipoDocumento = ?, correoElectronico = ?, fk_idRol = ?
                            WHERE numDocumento = ?
                            ''', [nombres, apellidos, documento, tipo_documento, correo, rol, documento])
                    con.commit()
                if con.total_changes > 0:
                    cursor.execute(''' UPDATE paciente
                                SET idPaciente = ?, genero = ?
                                WHERE idPaciente = ?
                                ''', [documento, genero, documento])
                    con.commit()

                    if con.total_changes > 0:
                        resp = {
                            'status': True,
                            'mensaje': 'Paciente acutalizado'
                        }
                    else:
                        resp = {
                            'status': False,
                            'mensaje': 'Paciente no pudo ser actualizado en Paciente'
                        }
                else:
                    resp = {
                        'status': False,
                        'mensaje': 'Paciente no pudo ser actualizado en Usuario'
                    }
            else:
                resp = {
                    'status': False,
                    'mensaje': 'Documento debe ser un numero'
                }
        else:
            resp = {
                'status': False,
                'mensaje': 'Todos los campos son requeridos'
            }

        return jsonify(resp)

    else:
        return redirect('/logout')




@admin.route("/pacientes/eliminar/<id_paciente>", methods=["GET", "POST"])
def eliminar_paciente(id_paciente):
    resp = {}
    if 'id_usuario' in session:
        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()

            cursor.execute('''SELECT estado
            FROM usuario 
            WHERE numDocumento = ? and fk_idrol = 3;
            ''', [id_paciente])
            row = cursor.fetchone()

            if row:
                if row['estado'] == 1:
                    cursor.execute(''' UPDATE usuario
                            SET estado = 0
                            WHERE numDocumento = ?
                            ''', [id_paciente])
                    con.commit()
                    resp = {
                        'estado': 'Deshabilitado',
                        'mensaje': 'Paciente Deshabilitado del sistema '
                    }
                    print(resp)
                elif row['estado'] == 0:
                    cursor.execute(''' UPDATE usuario
                            SET estado = 1
                            WHERE numDocumento = ?
                            ''', [id_paciente])
                    con.commit()
                    resp = {
                        'estado': 'Habilitado',
                        'mensaje': 'Paciente Habilitado en el sistema '
                    }
        return jsonify(resp)
    
    else:
       return redirect('/logout')

'''
RUTA PARA VER EL HISTORIAL DE CITAS 
'''

@admin.route('/paciente-historial/<id_paciente>')
def paciente_historial(id_paciente):
    if 'id_usuario' in session:
        resp = {}
        informacion = []

        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            cursor.execute('''SELECT c.idCita, c.fecha, e.nombreEspecialidad as especialidad, 
                up.numDocumento as documento_paciente, up.nombreUsuario as nombre_paciente, 
                up.apellidoUsuario as apellido_paciente, um.numDocumento as documento_medico, 
                um.nombreUsuario as nombre_medico, um.apellidoUsuario as apellido_medico, 
                c.comentario FROM Cita as c
                JOIN usuario AS up ON up.numDocumento = c.idPaciente
                JOIN paciente AS p ON p.idPaciente = up.numDocumento
                JOIN medico AS m ON m.idMedico = c.idMedico
                JOIN usuario as um ON um.numDocumento = m.idMedico
                JOIN especialidad as e ON e.idEspecialidad = m.fk_idEspecialidad
                WHERE c.idPaciente = ? AND c.estado = 'F'
                ORDER BY c.fecha DESC;
            ''', [id_paciente])

            rows = cursor.fetchall()

            if rows:
                for row in rows:
                    informacion.append({
                        'id_cita': row['idCita'],
                        'fecha': row['fecha'],
                        'especialidad': row['especialidad'],
                        'doc_paciente': row['documento_paciente'],
                        'nombre_paciente': row['nombre_paciente'],
                        'apellido_paciente':row['apellido_paciente'],
                        'doc_medico':row['documento_medico'],
                        'nombre_medico': row['nombre_medico'],
                        'apellido_medico':row['apellido_medico'],
                        'comentario': row['comentario']
                    })
                
                resp = {
                    'respuesta': True,
                    'mensaje': f'Historial de citas del paciente {id_paciente}',
                    'informacion': informacion                        
                    
                }
            else:
                resp = {
                    'respuesta': False,
                    'mensaje': f'No se encontró historial de citas para el paciente {id_paciente}',
                    'informacion': informacion

                }

            return jsonify(resp)
    else:
       return redirect('/logout')



'''

Ruta para crear al paciente 

'''


@admin.route("/<id>/pacientes/crear", methods=["GET", "POST"])
def crear_paciente(id):
    if 'id_usuario' in session:
        form = Crear_Paciente_Form()

        if form.validate_on_submit():
            nombres = escape(form.nombres_paciente.data)
            apellidos = escape(form.apellidos_paciente.data)
            tipo_doc = escape(form.tipo_documento_paciente.data)
            doc = escape(form.documento_paciente.data)
            genero = escape(form.genero_paciente.data)
            email = escape(form.email_paciente.data)
            password = escape(form.password_paciente.data)
            encrp = hashlib.sha256(password.encode())
            pass_enc = encrp.hexdigest()

            with sqlite3.connect(db) as con:
                cursor = con.cursor()
                cursor.execute(
                    '''SELECT correoElectronico, numDocumento 
                    FROM usuario 
                    WHERE correoElectronico = ? or numDocumento = ?''', [email, doc])
                if cursor.fetchone():
                    flash(
                        'El documento o correo del paciente ya se encuentra en la base de datos')

                else:
                    cursor.execute('''INSERT INTO usuario(nombreUsuario,apellidoUsuario,tipoDocumento,numDocumento,correoElectronico,password,estado,fk_idRol) 
                    VALUES(?,?,?,?,?,?,?,?)''', [
                        nombres, apellidos, tipo_doc, doc, email, pass_enc, 1, 3])
                    con.commit()
                    cursor.execute(
                        '''INSERT INTO paciente (idPaciente,genero) 
                        VALUES (?,?)''', [doc, genero])
                    con.commit()
                    flash('Paciente registrado con exito')

        return render_template('/admin/crear-paciente.html', id=id,
                               form=form)

    else:
        return redirect('/logout')



'''
RUTA MEDICO INICIAL
'''
@admin.route('/<id>/medicos', methods=['GET', 'POST'])
def admin_medicos(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        busqueda_medico_form = Busqueda_Medico(request.form)

        if busqueda_medico_form.validate_on_submit():
            dato_medico = request.form['buscar_medico']
            return redirect(url_for('.medicos_resultados', dato=dato_medico))

        return render_template('admin/admin-medicos.html', id=id, form_medico=busqueda_medico_form)
    else:
        return redirect('/logout')




@admin.route('/medicos-resultados/<dato>', methods=['GET'])
def medicos_resultados(dato):
    if 'id_usuario' in session:
        mensaje = ''
        encontrados = 0
        medicos = []
        resultados = {}

        with sqlite3.connect(db) as con:
            cursor = con.cursor()

            cursor.execute('''SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.estado, r.nombreRol, e.nombreEspecialidad
            FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol 
            join Medico as m on m.idMedico = u.numDocumento
            join Especialidad as e on e.idEspecialidad = m.fk_idEspecialidad 
            WHERE u.fk_idRol = 2 AND (LOWER(u.nombreUsuario) = LOWER(?) or LOWER(u.apellidoUsuario)= LOWER(?) or LOWER(u.numDocumento) = LOWER(?) or LOWER(u.correoElectronico) = LOWER(?));''', [
                dato.lower(), dato.lower(), dato.lower(), dato.lower()])

            rows = cursor.fetchall()
            if rows:
                columns = [column[0] for column in cursor.description]
                for row in rows:
                    cont = 0
                    encontrados += 1
                    medico = {}
                    for r in row:
                        medico[columns[cont]] = r
                        cont += 1
                    medicos.append(medico)
                # print(pacientes)

                resultados = {
                    'mensaje': 'Medico(s) encontrado(s)',
                    'medicos_encontrados': encontrados,
                    'resultado': medicos
                }
                return jsonify(resultados)

            else:
                resultados = {
                    'mensaje': 'Medico no encontrado',
                    'medicos_encontrados': encontrados,
                    'resultado': medicos
                }
                return jsonify(resultados)

    else:
        return redirect('/logout')


@admin.route('/medicos-consultar/<id_medico>')
def medicos_consultar(id_medico):
    if 'id_usuario' in session:
        resp = {}

        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            cursor.execute('''SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.fk_idrol, e.idEspecialidad
            FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol
            join Medico as m on m.idMedico = u.numDocumento
            join Especialidad as e on e.idEspecialidad = m.fk_idEspecialidad
            WHERE u.numDocumento = ? and u.fk_idrol = 2;
            ''', [id_medico])

            row = cursor.fetchone()

            if row:
                cursor.execute('''Select * FROM Especialidad
                ''')
                rows_esp = cursor.fetchall()
                #print(rows_esp)
                arr_esp = []
                for id_es in rows_esp:
                    arr_esp.append({'nombre':id_es['nombreEspecialidad'],'id':id_es['idEspecialidad']})
                resp = {
                    'respuesta': True,
                    'mensaje': 'Medico encontrado',
                    'informacion': {
                        'tipo_documento': row['tipoDocumento'],
                        'documento': row['numDocumento'],
                        'nombres': row['nombreUsuario'],
                        'apellidos': row['apellidoUsuario'],
                        'correo': row['correoElectronico'],
                        'especialidades' : arr_esp,
                        'especialidad_medico': row['idEspecialidad'],
                        'rol': row['fk_idRol']
                    }
                }
            else:
                resp = {
                    'respuesta': False,
                    'mensaje': 'Medico no encontrado',
                    'informacion': {}
                }

            return jsonify(resp)
    else:
        return redirect('/logout')


@admin.route('/medicos-editar', methods=['POST'])
def editar_medico():
    if 'id_usuario' in session:
        tipo_documento = request.form["editar_tipo_documento"]
        documento = request.form["editar_documento"]
        nombres = request.form["editar_nombres"]
        apellidos = request.form["editar_apellidos"]
        especialidad = request.form["editar_especialidad"]
        correo = request.form["editar_correo"]
        rol = request.form["editar_rol"]
        resp = {}

        if len(documento) > 0 and len(tipo_documento) > 0 and len(nombres) > 0 and len(apellidos) > 0 and len(especialidad) > 0 and len(correo) > 0 and len(rol) > 0:
            if(int(documento)):
                with sqlite3.connect(db) as con:
                    cursor = con.cursor()

                    cursor.execute(''' UPDATE usuario
                            SET nombreUsuario = ?, apellidoUsuario = ?, numDocumento = ?, tipoDocumento = ?, correoElectronico = ?, fk_idRol = ?
                            WHERE numDocumento = ?
                            ''', [nombres, apellidos, documento, tipo_documento, correo, rol, documento])
                    con.commit()
                if con.total_changes > 0:
                    cursor.execute(''' UPDATE medico
                                SET idMedico = ?, fk_idEspecialidad = ?
                                WHERE idMedico = ?
                                ''', [documento, especialidad, documento])
                    con.commit()

                    if con.total_changes > 0:
                        resp = {
                            'status': True,
                            'mensaje': 'Medico acutalizado'
                        }
                    else:
                        resp = {
                            'status': False,
                            'mensaje': 'Medico no pudo ser actualizado en Paciente'
                        }
                else:
                    resp = {
                        'status': False,
                        'mensaje': 'Medico no pudo ser actualizado en Usuario'
                    }
            else:
                resp = {
                    'status': False,
                    'mensaje': 'Documento debe ser un numero'
                }
        else:
            resp = {
                'status': False,
                'mensaje': 'Todos los campos son requeridos'
            }

        return jsonify(resp)

    else:
        return redirect('/logout')


@admin.route("/medicos/eliminar/<id_medico>", methods=["GET", "POST"])
def eliminar_medico(id_medico):
    resp = {}
    if 'id_usuario' in session:
        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()

            cursor.execute('''SELECT estado
            FROM usuario 
            WHERE numDocumento = ? and fk_idrol = 2;
            ''', [id_medico])
            row = cursor.fetchone()

            if row:
                if row['estado'] == 1:
                    cursor.execute(''' UPDATE usuario
                            SET estado = 0
                            WHERE numDocumento = ?
                            ''', [id_medico])
                    con.commit()
                    resp = {
                        'estado': 'Deshabilitado',
                        'mensaje': 'Medico Deshabilitado del sistema '
                    }
                    print(resp)
                elif row['estado'] == 0:
                    cursor.execute(''' UPDATE usuario
                            SET estado = 1
                            WHERE numDocumento = ?
                            ''', [id_medico])
                    con.commit()
                    resp = {
                        'estado': 'Habilitado',
                        'mensaje': 'Medico Habilitado en el sistema '
                    }
        return jsonify(resp)

    else:
        return redirect('/logout')


@admin.route('/medicos/especialidades',methods=['GET'])
def obtener_especialidades():
    if 'id_usuario' in session:
        resp = [{'id':'','nombre':'--Seleccionar especialidad--'}]
        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()

            cursor.execute('''SELECT * FROM especialidad''')

            rows = cursor.fetchall()
            for row in rows:
                resp.append({'id':row['idEspecialidad'],'nombre':row['nombreEspecialidad']})
        
        return jsonify(resp)
    else:
        return redirect('/logout')


@admin.route("/<id>/medicos/crear", methods=["GET"])
def crear_medico(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        return render_template('admin/crear-medico.html',id=id)
    else:
        return redirect('/logout')


@admin.route("/medicos/agregar", methods=["POST"])
def agregar_medico():
    if 'id_usuario' in session:
        resp = {}
        tipo_documento = request.form["tipo_documento_medico"]
        documento = request.form["documento_medico"]
        nombres = request.form["nombres_medico"]
        apellidos = request.form["apellidos_medico"]
        especialidad = request.form["especialidad_medico"]
        correo = request.form["email_medico"]
        password = request.form["password_medico"]
        encrp = hashlib.sha256(password.encode())
        pass_enc = encrp.hexdigest()

        if len(documento) > 0 and len(tipo_documento) > 0 and len(nombres) > 0 and len(apellidos) > 0 and len(especialidad) > 0 and len(correo) > 0 and len(password) > 0:
            if(int(documento)):
                with sqlite3.connect(db) as con:
                    cursor = con.cursor()
                    cursor.execute(
                    '''SELECT correoElectronico, numDocumento 
                    FROM usuario 
                    WHERE correoElectronico = ? or numDocumento = ?''', [correo, documento])
                    if cursor.fetchone():
                        resp = {'estado':False,'mensaje':'Usuario ya registrado en la base de datos'}
                    
                    else:
                        cursor.execute('''INSERT INTO usuario(nombreUsuario,apellidoUsuario,tipoDocumento,numDocumento,correoElectronico,password,estado,fk_idRol) 
                        VALUES(?,?,?,?,?,?,?,?)''', [
                            nombres, apellidos, tipo_documento, documento, correo, pass_enc, 1, 2])
                        con.commit()
                        cursor.execute(
                        '''INSERT INTO medico (idMedico,fk_idEspecialidad) 
                        VALUES (?,?)''', [documento, especialidad])
                        con.commit()
                        resp = {'estado':True,'mensaje':'Usuario registrado satisfactoriamente'}

            else:
                resp = {'estado':False,'mensaje':'El documento debe ser unicamente numerico'}
        else:
            resp = {'estado':False,'mensaje':'Todos los campos son requeridos'}
        
        return jsonify(resp)
    
    else:
        return redirect('/logout')                  


'''
RUTA ADMIN INFORMACION
'''

@admin.route('/<id>/informacion')
def informacion_camino(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        return render_template('admin/admin-informacion.html', id=id)
    else:
        return redirect('/logout')


'''
RUTAS ESTADISTICAS
'''
@admin.route("/estadistica1", methods=["GET"])
def estadisticas1():
    resp = {}
    with sqlite3.connect(db) as con:
        df = pd.read_sql_query(
            "select estado, count(*) as cantidad from cita group by estado;", con)
        df.set_index('estado', inplace=True)

        df.plot(kind='barh', subplots=True)

        plt.xticks(rotation=0)
        plt.title('Número de Citas de acuerdo al Estado')
        plt.savefig(f'{homeServer}static/img/Estadisticas/estado.jpg')

        resp = {
            'url': '/static/img/Estadisticas/estado.jpg',
            'titulo': 'Número de Citas de acuerdo al Estado'
        }
    return jsonify(resp)


@admin.route("/estadistica3", methods=["GET"])
def estadisticas3():
    resp = {}
    with sqlite3.connect(db) as con:
        df = pd.read_sql_query(
            "SELECT Calificacion, Count(*) as Porcentajes FROM CITA WHERE Calificacion IS NOT NULL GROUP BY Calificacion;", con)
        long = len(df.index)
        df.set_index('calificacion', inplace=True)
        # df.set_index('Calificacion')
        colores = ["#EE6055", "#60D394", "#AAF683", "#FFD97D", "#FF9B85"]

        if (long == 2):
            desfase = (0, 0.1)
        elif (long == 3):
            desfase = (0, 0, 0.1)
        elif (long == 4):
            desfase = (0, 0, 0, 0.1)
        elif (long == 5):
            desfase = (0, 0, 0, 0, 0.1)
        elif (long == 1):
            desfase = (0.1)
        df.plot(kind='pie', subplots=True, autopct="%0.1f %%", colors=colores)
        plt.xticks(rotation=0)
        plt.title('Nivel de Satisfaccion de los Pacientes')
        plt.savefig(f'{homeServer}static/img/Estadisticas/satisfaccion.jpg')
        resp = {
            'url': '/static/img/Estadisticas/satisfaccion.jpg',
            'titulo': 'Nivel de Satisfaccion de los Pacientes'
        }
    return jsonify(resp)


@admin.route("/estadistica4", methods=["GET"])
def estadisticas4():
    resp = {}
    with sqlite3.connect(db) as con:
        df = pd.read_sql_query(
            "SELECT nombreEspecialidad, Count(*) as o FROM CITA as c, especialidad as e WHERE c.Especialidad= e.idEspecialidad GROUP BY nombreEspecialidad;", con)
        long = len(df.index)
        df.set_index('nombreEspecialidad', inplace=True)

        # df.set_index('Calificacion')
        colores = ["#EE6055", "#60D394", "#AAF683", "#FFD97D", "#FF9B85"]
        if (long == 1):
            desfase = (0.1)
        elif (long == 2):
            desfase = (0, 0.1)
        elif (long == 3):
            desfase = (0, 0, 0.1)
        elif (long == 4):
            desfase = (0, 0, 0, 0.1)
        df.plot(kind='pie', subplots=True, autopct="%0.1f %%",
                colors=colores, explode=desfase)
        # df.plot(kind='bar')
        #print(df.head)
        plt.xticks(rotation=0)
        plt.title('Numero de Pacientes por Especialidad')
        plt.legend(df.index, loc=7)
        plt.savefig(f'{homeServer}static/img/Estadisticas/especialidad.jpg')
        resp = {
            'url': '/static/img/Estadisticas/especialidad.jpg',
            'titulo': 'Numero de Pacientes por Especialidad'
        }
    return jsonify(resp)


@admin.route("/estadistica5", methods=["GET"])
def estadisticas5():
    resp = {}
    with sqlite3.connect(db) as con:
        df1 = pd.read_sql_query(
            "SELECT nombreEspecialidad,Count(*) as M FROM CITA as c, especialidad as e , paciente as p WHERE c.Especialidad= e.idEspecialidad  and c.idPaciente=p.idPaciente and p.genero='M' GROUP BY nombreEspecialidad;", con)
        df2 = pd.read_sql_query(
            "SELECT nombreEspecialidad,Count(*) as F FROM CITA as c, especialidad as e , paciente as p WHERE c.Especialidad= e.idEspecialidad  and c.idPaciente=p.idPaciente and p.genero='F' GROUP BY nombreEspecialidad;", con)
        df = pd.merge(df1, df2, on='nombreEspecialidad', how='outer')
        df.set_index('nombreEspecialidad', inplace=True)
        # df.set_index('Calificacion')
        #print(df)
        colores = ["#EE6055", "#60D394", "#AAF683", "#FFD97D", "#FF9B85"]

        df.plot(kind='bar', stacked=True)
        # df.plot(kind='bar')
        #print(df)
        plt.xticks(rotation=0)
        plt.title('Numero de Pacientes por Especialidad y Género')
        plt.legend(['Masculino', 'Fenemino'], loc=7)
        plt.savefig(f'{homeServer}static/img/Estadisticas/espec_genero.jpg')

        resp = {
            'url': '/static/img/Estadisticas/espec_genero.jpg',
            'titulo': 'Numero de Pacientes por Especialidad y Género'
        }
    return jsonify(resp)




