from flask import Blueprint, json, render_template, redirect, request, jsonify, session, sessions
from flask.helpers import url_for
import sqlite3
import os

from forms.forms import Atender_Cita

doctor = Blueprint('doctor', __name__)
doctor.secret_key = os.urandom(32)
#Utilizar cuando se suba a la nube
#homeServer = "/home/pmonteroa/mysite/"
homeServer = ""
db = f'{homeServer}db/dbProyecto.db'


@doctor.route("/<id_medico>", methods=["GET"])
def init(id_medico):
    if 'id_usuario' in session and session['id_usuario'] == id_medico:
        with sqlite3.connect(db) as con:
            # convierte la respuesta de la consulta a diccionario
            con.row_factory = sqlite3.Row

            cursor = con.cursor()
            cursor.execute("""SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, e.nombreEspecialidad
                                FROM medico m
                                JOIN usuario u
                                    ON (m.idMedico = u.numDocumento)
                                JOIN especialidad e
                                    on (m.fk_idEspecialidad = e.idEspecialidad)
                                WHERE m.idMedico = ?""", [id_medico])

            info_perfil_medico = cursor.fetchone()
            print(info_perfil_medico)
            if info_perfil_medico:
                return render_template('/doctor/doctor-inicio.html', id=id_medico, info=info_perfil_medico)
            else:
                #flash("No existe informacion para el id consultado")
                return render_template('/doctor/doctor-inicio.html', id=id_medico,  info=None)
    else:
        return redirect('/logout')


# Ejemplo de llamado a este API: http://127.0.0.1:5000/doctor/agenda/100011/2021-10-20
@doctor.route('/agenda/<id_medico>/<fecha>', methods=['GET'])
def agenda_resultados(id_medico, fecha):
    if 'id_usuario' in session and session['id_usuario'] == id_medico:

        citas_dia = 0
        citas = []
        resultados = {}

        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                '''SELECT u.nombreUsuario, c.fecha, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, c.turno,u.estado, r.nombreRol, p.genero, c.comentario 
                        FROM ((usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol) 
                        join Paciente as p on p.idPaciente = u.numDocumento) 
                        join Cita as c on  p.idPaciente = c.idPaciente 
                        WHERE c.idMedico = ? and c.estado="P" and c.fecha=? order by c.turno asc;''',
                [id_medico, fecha])
        # cursor.execute('SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.estado, r.nombreRol, p.genero FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol join Paciente as p on p.idPaciente = u.numDocumento WHERE u.fk_idRol = 3 AND (LOWER(u.nombreUsuario) = LOWER(?) or LOWER(u.apellidoUsuario)= LOWER(?) or LOWER(u.numDocumento) = LOWER(?) or LOWER(u.correoElectronico) = LOWER(?));', [
            #               dato.lower(), dato.lower(), dato.lower(), dato.lower()])

            rows = cursor.fetchall()
            if rows:
                columns = [column[0] for column in cursor.description]
                # print(columns)
                for row in rows:
                    cont = 0
                    citas_dia += 1
                    cita = {}
                    for r in row:
                        cita[columns[cont]] = r
                        cont += 1
                    citas.append(cita)
            # print(citas)

            resultados = {
                'mensaje': 'Citas Para el dia de hoy',
                'citas_encontrados': citas_dia,
                'resultado': citas
            }
            return render_template('/doctor/doctor-agenda.html', id=id_medico, citas=citas, citas_dia=citas_dia)

    else:
        return redirect('/logout')

# Ejemplo de llamado a esta API: http://127.0.0.1:5000/doctor/historial-cita/100013


@doctor.route('/historial-cita/<id_medico>', methods=['GET'])
def pacientes_resultados(id_medico):
    if 'id_usuario' in session and session['id_usuario'] == id_medico:
        encontrados = 0
        citas = []
        resultados = {}

        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            cursor.execute(
                '''SELECT u.nombreUsuario, c.fecha, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, c.turno,u.estado, r.nombreRol, p.genero, c.comentario 
                FROM ((usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol) 
                join Paciente as p on p.idPaciente = u.numDocumento) 
                join Cita as c on  p.idPaciente = c.idPaciente 
                WHERE c.idMedico = ? and c.estado="F" order by c.fecha desc;''', [id_medico])
        # cursor.execute('SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.numDocumento, u.correoElectronico, u.estado, r.nombreRol, p.genero FROM usuario AS u JOIN rol AS r On r.idRol = u.fk_idRol join Paciente as p on p.idPaciente = u.numDocumento WHERE u.fk_idRol = 3 AND (LOWER(u.nombreUsuario) = LOWER(?) or LOWER(u.apellidoUsuario)= LOWER(?) or LOWER(u.numDocumento) = LOWER(?) or LOWER(u.correoElectronico) = LOWER(?));', [
            #               dato.lower(), dato.lower(), dato.lower(), dato.lower()])

            historial = cursor.fetchall()
            if historial:
                return render_template('doctor/historial-doctor.html', id=id_medico, historial=historial)
            else:
                return render_template('doctor/historial-doctor.html', id=id_medico, historial=[])
    else:
        return redirect('/logout')


# Ruta de atender de cita
# Mostrara la cita que se seleccionó para atender
# cuando finalice la cita, lo actualizará en la base de datos
@doctor.route('/agenda/<id_medico>/atender-cita/<paciente>/<fecha>', methods=['GET', 'POST'])
def atender_cita(id_medico, paciente, fecha):
    if 'id_usuario' in session and session['id_usuario'] == id_medico:
        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            # cursor.execute('SELECT c.fecha, c.estado, c.turno, u.nombreUsuario AS nombreMedico, u.ApellidoUsuario AS apellidoMedico, e.nombreespecialidad FROM cita AS c JOIN usuario AS u on u.numDocumento = c.idMedico JOIN especialidad AS e ON e.idEspecialidad = c.especialidad WHERE c.idPaciente = ? AND c.idMedico = ? AND c.fecha = ? AND c.estado = ?', [
            #              id, medico, fecha, 'P'])
            cursor.execute('''SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, c.idPaciente, c.fecha, c.estado, c.turno
                                FROM cita AS c 
                                JOIN usuario as u ON u.numDocumento = c.idPaciente 
                                WHERE c.idMedico=? AND c.idPaciente=? AND c.fecha =? AND c.estado="P";''', [
                id_medico, paciente, fecha])
            usuario = cursor.fetchone()
            if usuario:
                form = Atender_Cita()
                if form.validate_on_submit():
                    comentario = form.comentarios_cita.data
                    cursor.execute('''UPDATE Cita SET estado="F", comentario=? 
                                    WHERE idMedico=? AND idPaciente=? AND fecha =?;''', [
                        comentario, id_medico, paciente, fecha])
                    con.commit()
                    return redirect(url_for('.agenda_resultados', id_medico=id_medico, fecha=fecha))
            return render_template('doctor/atender-cita.html', id=id_medico, paciente=paciente, fecha=fecha, usuario=usuario, form=form)

    else:
        return redirect('/logout')

# Ejemplo de llamado a esta API (tener en cuenta que solo se podra agregar el comentario si la cita esta en estado='P'): http://127.0.0.1:5000/doctor/actualizar-cita/100013/123/2021-10-6/Este es un comentario realizado por el medico


@doctor.route('/actualizar-cita/<id>/<paciente>/<fecha>/<comentario>', methods=['POST'])
def actualizar_cita(id, paciente, fecha, comentario):
    if 'id_usuario' in session and session['id_usuario'] == id:
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            # cursor.execute('SELECT c.fecha, c.estado, c.turno, u.nombreUsuario AS nombreMedico, u.ApellidoUsuario AS apellidoMedico, e.nombreespecialidad FROM cita AS c JOIN usuario AS u on u.numDocumento = c.idMedico JOIN especialidad AS e ON e.idEspecialidad = c.especialidad WHERE c.idPaciente = ? AND c.idMedico = ? AND c.fecha = ? AND c.estado = ?', [
            #              id, medico, fecha, 'P'])
            cursor.execute('SELECT idPaciente FROM Cita WHERE idMedico=? AND idPaciente=? AND fecha =? AND estado="P";', [
                id, paciente, fecha])
            if cursor.fetchone():

                cursor.execute('UPDATE Cita SET estado="F", comentario=? WHERE idMedico=? AND idPaciente=? AND fecha =?;', [
                    comentario, id, paciente, fecha])
                con.commit()

                return jsonify({'mensaje': 'Cita Actualizada', 'actualizada': True})

            return jsonify({'mensaje': 'No se pudo Actualizar la Cita', 'actualizada': False})
    else:
        return redirect('/logout')


@doctor.route('/historial-cita/<id_medico>/ver-cita/<paciente>/<fecha>', methods=['GET'])
def ver_cita(id_medico, paciente, fecha,):
    if 'id_usuario' in session and session['id_usuario'] == id_medico:
        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            respuesta = {}
            cursor.execute('''SELECT u.nombreUsuario, u.apellidoUsuario, u.tipoDocumento, u.correoElectronico, c.idPaciente, c.fecha, c.estado, c.comentario, p.genero
                                    FROM cita AS c 
                                    JOIN usuario as u ON u.numDocumento = c.idPaciente 
                                    Join paciente as P ON p.idPaciente = c.idPaciente
                                    WHERE c.idMedico=? AND c.idPaciente=? AND c.fecha =? AND c.estado="F";''', [
                id_medico, paciente, fecha])

            row = cursor.fetchone()
            if row:
                respuesta = {
                    'mensaje': 'Cita',
                    'estado': True,
                    'info': {
                        'nombre_paciente': row['nombreUsuario'],
                        'apellido_paciente': row['apellidoUsuario'],
                        'tipo_documento': row['tipoDocumento'],
                        'num_documento': row['idPaciente'],
                        'email': row['correoElectronico'],
                        'genero': row['genero'],
                        'fecha': row['fecha'],
                        'estado': row['estado'],
                        'comentario': row['comentario']
                    }
                }
            else:
                respuesta = {
                    'mensaje': 'Error: No hay informacion para esta cita',
                    'estado': False,
                    'info': {}
                }
        return jsonify(respuesta)
        
    else:
        return redirect('/logout')
