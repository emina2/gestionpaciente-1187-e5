from flask import Blueprint, json, render_template, redirect, request, jsonify, session, flash
from forms.forms import Confirmar_cita_form, EditarCitaForm
from flask.helpers import url_for
import sqlite3
import os
from werkzeug.utils import escape
from datetime import datetime

paciente = Blueprint('paciente', __name__)
paciente.secret_key = os.urandom(32)

#Constante para el maximo de citas que se podran por dia para el mismo medico
MAXIMO_TURNOS_ATENCION_POR_DIA = 20

#Utilizar cuando se suba a la nube
#homeServer = "/home/pmonteroa/mysite/"
homeServer = ""
db = f'{homeServer}db/dbProyecto.db'

'''
Ruta con session
'''


@paciente.route("/<id>", methods=["GET"])
def init(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                'SELECT nombreUsuario, apellidoUsuario, numDocumento FROM usuario where numDocumento = ? and fk_idRol = ?', [id, 3])
            row = cursor.fetchone()
            if row:
                columns = [column[0] for column in cursor.description]
                # print(columns)
                usuario = dict(zip(columns, row))
            return render_template('/paciente-view/paciente-inicio.html', id=id)

    else:
        return redirect('/logout')


@paciente.route("/editar-cita/<id_paciente>/<id_cita>", methods = ["GET", "POST"])
def editar_cita(id_paciente, id_cita):
    if 'id_usuario' in session and session['id_usuario'] == id:
        frmEditarCita = EditarCitaForm()

        #Cuando se oprime el botón "Cancelar" regresa a la vista anterior
        if request.method == "POST":
            if frmEditarCita.btnCancelar.data:
                return redirect(url_for('.citas_pendientes', id=id_paciente))

        with sqlite3.connect(db) as con:
            #convierte la respuesta de la consulta a diccionario
            con.row_factory = sqlite3.Row
            cursor = con.cursor()
            cursor.execute("SELECT idCita, idPaciente, idMedico, fecha, estado, turno, especialidad FROM cita WHERE idCita = ?", [id_cita])

            #Colocamos fetchone debido a que sólo espero uno
            rowCita = cursor.fetchone()

            if rowCita:
                #Consultamos los medicos que coincidan con la especialidad que necesitamos
                cursor.execute("""
                                select u.nombreUsuario, u.apellidoUsuario, m.idMedico
                                from medico m
                                join usuario u
                                    on u.numDocumento = m.idMedico
                                where u.estado = 1
                                    and m.fk_idEspecialidad = ?
                            """, [rowCita["especialidad"]])

                frmEditarCita.medico_id_select.choices = [(medico["idMedico"], f"{medico['nombreUsuario']} {medico['apellidoUsuario']}") for medico in cursor.fetchall()]
                frmEditarCita.medico_id_select.default = rowCita["idMedico"]

                frmEditarCita.especialidad_id_select.default = rowCita["especialidad"]

                #frmEditarCita.fecha_cita.data = datetime.strptime("22/10/2022","%d/%m/%Y")
                #frmEditarCita.fecha_cita.default = datetime.strptime("22/10/2022","%d/%m/%Y")
                #frmEditarCita.fecha_cita.data = datetime.today
                
                #Esto toca hacerlo para que se seleccione el valor del select en la vista y toca antes de establecer los demás valores o sino los deja vacios por alguna extraña razón
                frmEditarCita.process()
                frmEditarCita.paciente_id.data = rowCita["idPaciente"]
                
                
                

        #form = UserForm()
        #form.job.choices = [(a.id, a.name) for a in Job.query.order_by(Job.name)]

        


        return render_template("paciente-view/editarCita.html", frm = frmEditarCita, id = id_paciente)
    else:
        return redirect('/logout')


'''
Ruta con session
'''
# Ruta paciente cuando selecciona una especialidad para crear una nueva cita


@paciente.route('/nueva-cita/<especialidad>/<id>', methods=['GET', 'POST'])
def nueva_cita(especialidad, id):
    # cita_form = Nueva_cita_form(request.form)

    if 'id_usuario' in session and session['id_usuario'] == id:
        medicos_listado = []
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                'SELECT medico.idMedico, usuario.nombreUsuario, usuario.apellidoUsuario, especialidad.nombreEspecialidad FROM medico JOIN usuario ON usuario.numDocumento = medico.idMedico JOIN especialidad ON especialidad.idEspecialidad = medico.fk_idEspecialidad WHERE especialidad.idEspecialidad = ?', [especialidad])

            rows_especialidad = cursor.fetchall()
            if rows_especialidad:
                columns = [column[0] for column in cursor.description]
                for row in rows_especialidad:
                    medicos_listado.append(dict(zip(columns, row)))

        confirmar_form = Confirmar_cita_form(request.form)
        if especialidad == '1':
            especialidad = 'Medicina General'
        elif especialidad == '2':
            especialidad = 'Odontologia'
        elif especialidad == '3':
            especialidad = 'Oftalmologia'

        if confirmar_form.validate_on_submit():
            fecha = escape(confirmar_form.confirmar_fecha_cita.data)
            espec_cita = escape(confirmar_form.confirmar_especialidad_cita.data)
            medico = escape(confirmar_form.confirmar_medico_cita.data)
            turno = escape(confirmar_form.confirmar_turno_cita.data)
            cita_valida = True
            cita = []
            turnos = []

            with sqlite3.connect(db) as con:

                cursor = con.cursor()
                # traemos todos los resultados para la cita en la fecha y con el medico indicado
                cursor.execute(
                    'SELECT idCita, idPaciente, idMedico, fecha, estado, comentario, calificacion, turno, especialidad FROM CITA WHERE fecha = ? AND idMedico = ? AND estado = ? ORDER BY turno', [fecha, medico, 'P'])
                rowsCitas = cursor.fetchall()
                turno = 1
                # validamos si hay resultados en citas
                if rowsCitas:
                    columns = [column[0] for column in cursor.description]
                    # guardamos todos los turnos en una arrglo para asignar los turnos
                    rowInfoCita = {}
                    for row in rowsCitas:
                        rowInfoCita = dict(zip(columns, row))
                        turnos.append(rowInfoCita['turno'])
                    # asignamos el turno, si el turno ya esta en la lista, aumente en uno hasta que se acabe la lista
                    # si el turno no esta, se asigna y termina el ciclo
                    for t in turnos:
                        if turno == int(t):
                            turno += 1
                        else:
                            turno = turno
                            break

                    #Validamos que no se excedan los turnos permitidos para atencion del médico
                    if turno > MAXIMO_TURNOS_ATENCION_POR_DIA:
                        flash(f'Ya se cumplieron todos los turnos disponibles para la fecha {fecha}. Por favor intente nuevamente.s')
                        return render_template('paciente-view/nuevacita.html',
                                especialidad=especialidad,
                                id=id,
                                medicos=medicos_listado,
                                conf_form=confirmar_form)
                    else:
                        cursor.execute('INSERT INTO Cita (idPaciente,idMedico,especialidad,fecha,estado,turno) VALUES (?,?,?,?,?,?)', [
                            id, medico, espec_cita, fecha, 'P', turno])
                        con.commit()
                # en caso que no haya resultados de cita para ese dia y con ese medico, creamos una nueva cita
                else:
                    cursor.execute('INSERT INTO Cita (idPaciente,idMedico,especialidad,fecha,estado,turno) VALUES (?,?,?,?,?,?)', [
                        id, medico, espec_cita, fecha, 'P', turno])
                    con.commit()
            return redirect(url_for('.citas_pendientes', id=id))

        return render_template('paciente-view/nuevacita.html',
                                especialidad=especialidad,
                                id=id,
                                medicos=medicos_listado,
                                conf_form=confirmar_form)
    else:
        return redirect('/logout')


@paciente.route('/validar-cita/<fecha>/<especialidad>/<medico>/<id>', methods=['GET'])
def validar_cita(fecha, especialidad, medico, id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        cita = []
        turnos = []

        with sqlite3.connect(db) as con:
            cursorPaciente = con.cursor()
            cursor = con.cursor()

            # Consulta para conocer si el usuario ya tiene una cita pendiente para ese dia con ese medico
            cursorPaciente.execute(
                'SELECT * FROM CITA WHERE idPaciente = ? AND fecha = ? AND idMedico = ? AND estado = ?', [id, fecha, medico, 'P'])
            if cursorPaciente.fetchall():
                cita = {
                    'disponible': False,
                    'mensaje': f"Ya tiene una cita asiganda para el dia '{fecha}' con este medico",
                    'informacion': {}
                }
                return jsonify(cita)

            # traemos todos los resultados para la cita en la fecha y con el medico indicado
            cursor.execute(
                'SELECT idCita, idPaciente, idMedico, fecha, estado, comentario, calificacion, turno, especialidad FROM CITA WHERE fecha = ? AND idMedico = ? AND estado = ? ORDER BY turno', [fecha, medico, 'P'])

            rowsCitas = cursor.fetchall()
            turno = 1

            # validamos si hay resultados en citas
            if rowsCitas:
                columns = [column[0] for column in cursor.description]
                print(columns)
                print(f'cantidad: {len(rowsCitas)}')
                # si los resultados son mayor a 20, no agregará la cita
                if len(rowsCitas) >= MAXIMO_TURNOS_ATENCION_POR_DIA:
                    cita = {
                        'disponible': False,
                        'mensaje': f'No hay cita disponible para el día "{fecha}"',
                        'informacion': {}
                    }
                else:
                    # guardamos todos los turnos en una arrglo para asignar los turnos
                    rowInfoCita = {}
                    for row in rowsCitas:
                        rowInfoCita = dict(zip(columns, row))
                        turnos.append(rowInfoCita['turno'])
                    # asignamos el turno, si el turno ya esta en la lista, aumente en uno hasta que se acabe la lista
                    # si el turno no esta, se asigna y termina el ciclo
                    for t in turnos:
                        if turno == int(t):
                            turno += 1
                        else:
                            turno = turno
                            break
                    cita = {
                        'disponible': True,
                        'mensaje': 'Cita disponible',
                        'informacion': {
                            'fecha': fecha,
                            'turno': turno,
                            'especialidad': especialidad,
                            'medico': medico,
                            'id_paciente': id,
                            'ciudad': 'Barranquilla'
                        }
                    }
            # en caso que no haya resultados de cita para ese dia y con ese medico, creamos una nueva cita
            else:
                cita = {
                    'disponible': True,
                    'mensaje': 'Cita disponible',
                    'informacion': {
                        'fecha': fecha,
                        'turno': turno,
                        'especialidad': especialidad,
                        'medico': medico,
                        'id_paciente': id,
                        'ciudad': 'Barranquilla'
                    }
                }

            return jsonify(cita)
    else:
        return redirect('/logout')


'''
Ruta con session
'''
# Ruta paciente para acceder a las citas


@paciente.route('/mis-citas/<id>', methods=['GET'])
def mis_citas(id):
    if 'id_usuario' in session and session['id_usuario'] == id:

        return render_template('paciente-view/miscitas.html', id=id)

    else:
        return redirect('/logout')


@paciente.route('/mis-citas/citas-pendientes/<id>', methods=['GET'])
def citas_pendientes(id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        citas_usuario = []
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                'SELECT c.idCita, c.fecha, c.estado, c.turno, c.idMedico, u.nombreUsuario AS nombreMedico, u.ApellidoUsuario AS apellidoMedico, e.nombreespecialidad FROM cita AS c JOIN usuario as u ON u.numDocumento = c.idMedico JOIN especialidad AS e on e.idEspecialidad = c.especialidad WHERE c.idPaciente = ? AND c.estado = ?', [id, 'P'])

            rows_pendientes = cursor.fetchall()
            if rows_pendientes:
                columns = [column[0] for column in cursor.description]
                citas = {}
                for row in rows_pendientes:
                    citas = dict(zip(columns, row))
                    citas_usuario.append(citas)
                print(citas_usuario)

        return render_template('paciente-view/citaspendientes.html', id=id, citas=citas_usuario)
    
    else:
        return redirect('/logout')


#Ejemplo de uso: http://127.0.0.1:5000/paciente/calificar-cita/3/4
@paciente.route('/calificar-cita/<int:id_cita>/<int:calificacion>', methods = ['GET'])
def calificar_cita(id_cita, calificacion):

    if 'id_usuario' in session:
        with sqlite3.connect(db) as con:
            cursor = con.cursor()

            resp = {}

            #Verificamos que exista la cita y que esté finalizada para que el usuario la pueda calificar
            cursor.execute("""SELECT idCita
                                FROM cita c
                                WHERE estado = 'F' and idCita = ? and idPaciente = ?""", [id_cita, session['id_usuario']])

            if cursor.fetchone():
                cursor.execute("""UPDATE cita 
                                SET calificacion = ?
                                WHERE idCita = ?""", [calificacion, id_cita])
                con.commit()

                resp = {
                    'respuesta': True,
                    'mensaje': 'Cita calificada exitosamente'
                }
            
            else:
                resp = {
                    'respuesta': False,
                    'mensaje': f'No se encontró la cita {id_cita} en estado finalizada para poder realizar la calificación'
                }

            return jsonify(resp)

    else:
        return redirect('/logout')


#Ejemplo de uso: http://127.0.0.1:5000/paciente/obtener-comentario/1
@paciente.route('/obtener-comentario/<int:id_cita>', methods = ['GET'])
def obtener_comentario_por_cita(id_cita):
    if 'id_usuario' in session:
        with sqlite3.connect(db) as con:
            con.row_factory = sqlite3.Row
            cursor = con.cursor()

            resp = {}

            cursor.execute('''SELECT idCita, idPaciente, idMedico, estado, comentario
                                FROM cita c
                                WHERE idCita = ?  and idPaciente = ?''', [id_cita, session['id_usuario']])

            row = cursor.fetchone()

            if row:
                #Verificamos que existan comentarios para la cita encontrada
                if row['comentario']:
                    resp = {
                        'respuesta': True,
                        'mensaje': 'Comentario encontrado',
                        'informacion': {
                            'estado_cita': row['estado'],
                            'id_cita': row['idCita'],
                            'comentario': row['comentario']
                        }
                    }
                else:
                    resp = {
                        'respuesta': True,
                        'mensaje': 'Comentario encontrado',
                        'informacion': {
                            'estado_cita': row['estado'],
                            'id_cita': row['idCita'],
                            'comentario': ''
                        }
                    }
            else:
                resp = {
                    'respuesta': False,
                    'mensaje': f'No se encontró información para la cita {id_cita}',
                    'informacion': {}
                }

            return jsonify(resp)
    
    else:
        return redirect('/logout')


@paciente.route('/mis-citas/citas-canceladas-finalizadas/<id>', methods=['GET'])
def citas_canceladas_finalizadas(id):

    if 'id_usuario' in session and session['id_usuario'] == id:
        citas_usuario = []

        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                'SELECT c.idCita, c.calificacion, c.idPaciente, c.fecha, c.estado, c.turno, u.nombreUsuario AS nombreMedico, u.ApellidoUsuario AS apellidoMedico, e.nombreespecialidad FROM cita AS c JOIN usuario as u ON u.numDocumento = c.idMedico JOIN especialidad AS e on e.idEspecialidad = c.especialidad WHERE c.idPaciente = ? AND (c.estado = ? or c.estado = ?)', [id, 'C', 'F'])

            rows_pendientes = cursor.fetchall()
            print(id)
            print(rows_pendientes)
            if rows_pendientes:
                columns = [column[0] for column in cursor.description]
                citas = {}
                for row in rows_pendientes:
                    citas = dict(zip(columns, row))
                    citas_usuario.append(citas)
                print(citas_usuario)
        return render_template('paciente-view/citascanceladasfinalizadas.html', id=id, citas=citas_usuario)
    
    else:
        return redirect('/logout')


@paciente.route('/mis-citas/cancelar-cita/<fecha>/<medico>/<id>', methods=['GET'])
def cancelar_cita(fecha, medico, id):
    if 'id_usuario' in session and session['id_usuario'] == id:
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute('SELECT c.fecha, c.estado, c.turno, u.nombreUsuario AS nombreMedico, u.ApellidoUsuario AS apellidoMedico, e.nombreespecialidad FROM cita AS c JOIN usuario AS u on u.numDocumento = c.idMedico JOIN especialidad AS e ON e.idEspecialidad = c.especialidad WHERE c.idPaciente = ? AND c.idMedico = ? AND c.fecha = ? AND c.estado = ?', [
                        id, medico, fecha, 'P'])

            if cursor.fetchone():
                cursor.execute('UPDATE Cita SET estado = ? WHERE idPaciente = ? and idMedico = ? and fecha = ? and estado = ?', [
                            'C', id, medico, fecha, 'P'])
                con.commit()

                return jsonify({'mensaje': 'Cita Cancelada', 'cancelada': True})

            return jsonify({'mensaje': 'No se pudo Cancelar Cita', 'cancelada': False})
    else:
        return redirect('/logout')
