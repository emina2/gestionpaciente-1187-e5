console.log('conectado');

let $btnAgenda = document.querySelector('.js-btn-agenda');

let url = `${location.origin}/doctor`;

let fecha = new Date();
let dia = fecha.getDate(),
  mes = fecha.getMonth(),
  anio = fecha.getFullYear(),
  fechaActual = `${anio}-${mes + 1}-${dia}`;

//Añadirá la fecha actual al enlace de Agenda para las citas del dia
const asignarEnlaceAgenda = () => {
  let enlaceAgenda = $btnAgenda.getAttribute('href');
  $btnAgenda.setAttribute('href', `${enlaceAgenda}/${fechaActual}`);
};

document.addEventListener('DOMContentLoaded', (e) => {
  asignarEnlaceAgenda();
});

/**MODAL HISTORIAL CITA */
//
let $modalContainer = document.querySelector('.container-ver-historia-cita'),
  $mensajeModal = document.querySelector('.mensaje-modal-ver'),
  $btnCerrarModal = document.querySelector('.btn-cerrar-ver');

const verInformacionCita = (data) => {
  if (data.estado) {
    $mensajeModal.innerHTML = `
    <p><strong>Fecha Cita:</strong> ${data.info.fecha}</p>
    <p><strong>Estado Cita:</strong> ${data.info.estado === 'F' ? 'Finalizada' : ''}</p>
    <p><strong>Nombres Paciente:</strong> ${data.info.nombre_paciente}</p>
    <p><strong>Apellidos Paciente:</strong> ${data.info.apellido_paciente}</p>
    <p><strong>Tipo Documento:</strong> ${data.info.tipo_documento}</p>
    <p><strong>Documento del Paciente:</strong> ${data.info.num_documento}</p>
    <p><strong>Email:</strong> ${data.info.email}</p>
    <p><strong>Genero:</strong> ${data.info.genero === 'F' ? 'Femenino' : 'Masculino'}</p>
    <p><strong>Comentarios:</strong> </p>
    <p>${data.info.comentario}</p>
    `;
  } else {
    $mensajeModal.textContent = data.mensaje;
  }
};

const fetchVerCita = async (url, id_medico, id_paciente, fecha) => {
  try {
    let res = await fetch(`${url}/historial-cita/${id_medico}/ver-cita/${id_paciente}/${fecha}`);

    if (!res.ok) throw new Error();
    let json = await res.json();
    console.log(json);
    verInformacionCita(json);
  } catch (error) {
    console.log(error);
  }
};

document.addEventListener('click', (e) => {
  //abrimos la ventama modal
  if (e.target.matches('.btn-ver-cita')) {
    $modalContainer.classList.add('active');
    //obtenemos los datos del btn para pasarselos a la funcion
    let id_medico = e.target.dataset.id,
      id_paciente = e.target.dataset.paciente,
      fecha = e.target.dataset.fecha;

    fetchVerCita(url, id_medico, id_paciente, fecha);
  }

  //Cerramos la ventana modal
  if (e.target.matches('.btn-cerrar-ver')) {
    $modalContainer.classList.remove('active');
    $mensajeModal.innerHTML = '';
  }
});
