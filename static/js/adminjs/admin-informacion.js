console.log('conectado');

const $containerModalInfo = document.querySelector('.container-modal-admin-info'),
  $mensajeModalInfo = document.querySelector('.mensaje-modal-admin-info'),
  $imgModalInfo = document.querySelector('.img-modal-info'),
  $tituloModalInfo = document.querySelector('.titulo-modal-info'),
  $loader = document.querySelector('.container-loader-admin-info');

const url = location.origin;

const mostrarModal = (url, titulo) => {
  $containerModalInfo.classList.add('active');
  $imgModalInfo.setAttribute('src', url);
  $imgModalInfo.setAttribute('alt', titulo);
  $tituloModalInfo.textContent = titulo;
};

const obtenerEstadistica = async (url, urlEstadistica) => {
  try {
    $loader.classList.add('active');
    let res = await fetch(`${url}/${urlEstadistica}`);
    if (!res.ok) throw { mensaje: res.statusText, status: res.status };
    let json = await res.json();
    mostrarModal(json.url, json.titulo);
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

document.addEventListener('click', (e) => {
  if (e.target.matches('.btn-admin-info')) {
    let btnUrl = e.target.dataset.route;
    obtenerEstadistica(url, btnUrl);
  }
  if (e.target.matches('.btn-cerrar-modal-admin-info')) {
    $containerModalInfo.classList.remove('active');
  }
});

document.addEventListener('keyup', (e) => {
  if (e.key === 'Escape') {
    $containerModalInfo.classList.remove('active');
  }
});
