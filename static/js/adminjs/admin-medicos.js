console.log('conectado');

let $formMedicoBusqueda = document.getElementById('medico-busqueda-form'),
  $mensaje = document.querySelector('.informacion-usuario-encontrados'),
  $tbody = document.querySelector('.tbody-usuarios'),
  $template = document.querySelector('.template-tabla').content,
  $fragment = document.createDocumentFragment(),
  $loader = document.querySelector('.loader'),
  $modalVerMedico = document.querySelector('.container-ver-medico'),
  $mensajeModalVer = document.querySelector('.mensaje-modal-ver-medico');

const url = location.origin;

//recibimos un array con todos los medicos encontrados
//Mostramos los datos en la tabla
const mostrarDatosTabla = (medicos) => {
  //console.log(medicos);
  $tbody.innerHTML = '';
  $mensaje.innerHTML = '';
  medicos.forEach((medico) => {
    let $clone = document.importNode($template, true);
    $clone.querySelector('.td_nombres').textContent = medico.nombreUsuario;
    $clone.querySelector('.td_apellidos').textContent = medico.apellidoUsuario;
    $clone.querySelector('.td_correo').textContent = medico.correoElectronico;
    $clone.querySelector('.td_tipo-documento').textContent = medico.tipoDocumento;
    $clone.querySelector('.td_documento').textContent = medico.numDocumento;
    $clone.querySelector('.btn-ver').dataset.nombres = medico.nombreUsuario;
    $clone.querySelector('.btn-ver').dataset.apellidos = medico.apellidoUsuario;
    $clone.querySelector('.btn-ver').dataset.tipodoc = medico.tipoDocumento;
    $clone.querySelector('.btn-ver').dataset.documento = medico.numDocumento;
    $clone.querySelector('.btn-ver').dataset.email = medico.correoElectronico;
    $clone.querySelector('.btn-ver').dataset.especialidad = medico.nombreEspecialidad;
    $clone.querySelector('.btn-ver').dataset.estado = medico.estado;
    $clone.querySelector('.btn-ver').dataset.tipousuario = medico.nombreRol;
    $clone.querySelector('.btn-editar').dataset.idmedico = medico.numDocumento;
    $clone.querySelector('.btn-estado').dataset.idmedico = medico.numDocumento;
    if (medico.estado === 1) {
      $clone.querySelector('.btn-estado').textContent = '❌ Borrar';
    } else {
      $clone.querySelector('.btn-estado').textContent = '🔄 Activar';
    }
    $clone.querySelector('.btn-estado').dataset.estado = medico.estado;

    $fragment.append($clone);
  });
  $tbody.append($fragment);
};

const mostrarDatosMedico = (datos) => {
  $modalVerMedico.classList.add('active');
  $mensajeModalVer.innerHTML = '';
  $mensajeModalVer.innerHTML = `
  <h4>Nombres: ${datos.nombres}</h4>
  <h4>Apellidos: ${datos.apellidos}</h4>
  <h4>Tipo Documento: ${datos.tipo_doc}</h4>
  <h4>Documento: ${datos.documento}</h4>
  <h4>Email: ${datos.email}</h4>
  <h4>Tipo usuario: ${datos.tipousuario}</h4>
  <h4>Especialidad: ${datos.especialidad}</h4>
  <h4>Estado: ${datos.estado === '1' ? 'Activo' : 'Inactivo'}</h4>
  `;
};

//Consumimos la api
//la variable url es la ip de la pagina, la variable dato es lo que buscará
const fetchMedicos = async (url, dato) => {
  $loader.classList.add('active');
  try {
    let res = await fetch(`${url}/admin/medicos-resultados/${dato}`);
    //console.log(res);
    if (!res.ok) {
      throw new Error('No se encontró la pagina');
    }
    let data = await res.json();
    //console.log(data);
    //la respuesta trae una propiedad llamada medicos_encontrados
    if (data.medicos_encontrados === 0) {
      $mensaje.innerHTML = '';
      $tbody.innerHTML = '';
      $mensaje.innerHTML = `<h3 class="informacion-usuario-mensaje">${data.mensaje}</h3 >`;
    } else {
      mostrarDatosTabla(data.resultado);
    }
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

/****************************EDITAR Medico****************************** */
let $formEditarMedico = document.querySelector('.form-editar-medico'),
  $modalEditar = document.querySelector('.container-modal-editar'),
  $modalConfirmacionEditar = document.querySelector('.container-modal-confirmar-editar'),
  $mensajeConfirmar = document.querySelector('.mensaje-modal-confirmar');

const validarInputs = (input, dato) => {
  if (input.value.trim() === '') {
    console.log(`${dato} es requerido`);
    return false;
  }
};

const llenarFormularioEditar = (medico) => {
  medico.especialidades.forEach((el) => {
    let $opt = document.createElement('option');
    $opt.value = el.id;
    $opt.text = el.nombre;
    $formEditarMedico.editar_especialidad.append($opt);
  });
  $formEditarMedico.editar_tipo_documento.value = medico.tipo_documento;
  $formEditarMedico.editar_documento.value = medico.documento;
  $formEditarMedico.editar_nombres.value = medico.nombres;
  $formEditarMedico.editar_apellidos.value = medico.apellidos;

  $formEditarMedico.editar_especialidad.value = medico.especialidad_medico;
  $formEditarMedico.editar_correo.value = medico.correo;
  $formEditarMedico.editar_rol.value = medico.rol;
};

const getMedicoFetch = async (url, id) => {
  try {
    $loader.classList.add('active');
    let res = await fetch(`${url}/admin/medicos-consultar/${id}`);

    if (!res.ok) {
      throw new Error('No hay respuesta');
    }

    let data = await res.json();
    //console.log(data);
    llenarFormularioEditar(data.informacion);
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

const editarMedicoFetch = async (url, form) => {
  try {
    $loader.classList.add('active');
    let $form = new FormData(form);
    let options = {
      method: 'POST',
      body: $form,
    };
    let res = await fetch(`${url}/admin/medicos-editar`, options);

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    let data = await res.json();

    $modalEditar.classList.remove('active');
    $modalConfirmacionEditar.classList.add('active');
    $mensajeConfirmar.innerHTML = `<p>${data.mensaje}</p>`;
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

/****************************ELIMINAR PACIENTE****************************** */
let $modalEliminar = document.querySelector('.container-modal-confirmar-eliminar'),
  $mensajeModalEliminar = document.querySelector('.mensaje-modal-confirmar-eliminar'),
  $btnEliminar = document.querySelector('.btn-eliminar-submit');

const mostrarModalEliminar = (id, estado) => {
  $modalEliminar.classList.add('active');
  $btnEliminar.dataset.idmedico = id;
  if (estado === '1') {
    $mensajeModalEliminar.textContent = '¿Desea deshabilitar al medico?';
  } else {
    $mensajeModalEliminar.textContent = '¿Desea habilitar al medico?';
  }
};

const eliminarMedicoFetch = async (url, id) => {
  try {
    let res = await fetch(`${url}/admin/medicos/eliminar/${id}`);

    if (!res.ok) throw { mensaje: res.statusText, status: res.status };

    let json = await res.json();
  } catch (error) {
    console.log(`${error.status}: ${error.mensaje}`);
  }
};

/*************************************EVENTOS************************************** */

document.addEventListener('click', (e) => {
  if (e.target.matches('.btn-ver')) {
    let $btn = e.target;
    console.log(e.target);
    let datos = {
      nombres: $btn.dataset.nombres,
      apellidos: $btn.dataset.apellidos,
      tipo_doc: $btn.dataset.tipodoc,
      documento: $btn.dataset.documento,
      email: $btn.dataset.email,
      estado: $btn.dataset.estado,
      especialidad: $btn.dataset.especialidad,
      tipousuario: $btn.dataset.tipousuario,
    };
    mostrarDatosMedico(datos);
  }

  if (e.target.matches('.btn-cerrar-ver-medico') || e.target.matches('.btn-cerrar-ver-medico *')) {
    $modalVerMedico.classList.remove('active');
  }

  if (e.target.matches('.btn-editar')) {
    e.stopPropagation();
    $modalEditar.classList.add('active');
    let id = e.target.dataset.idmedico;
    getMedicoFetch(url, id);
  }

  if (e.target.matches('.btn-editar-cancelar')) {
    $formEditarMedico.reset();
    $modalEditar.classList.remove('active');
  }

  if (e.target.matches('.btn_editar_submit')) {
    editarMedicoFetch(url, $formEditarMedico);
  }

  if (e.target.matches('.btn-cerrar-confirmar')) {
    $modalConfirmacionEditar.classList.remove('active');
    location.reload();
  }

  if (e.target.matches('.btn-estado')) {
    let id = e.target.dataset.idmedico;
    let estado = e.target.dataset.estado;
    mostrarModalEliminar(id, estado);
  }

  if (e.target === $btnEliminar) {
    let id = e.target.dataset.idmedico;
    console.log(id);
    eliminarMedicoFetch(url, id);
    location.reload();
  }

  if (e.target.matches('.btn-eliminar-cancelar')) {
    $modalEliminar.classList.remove('active');
    $btnEliminar.dataset.idpaciente = '';
  }
});

$formEditarMedico.editar_documento.addEventListener('change', (e) => {
  $modalEditar.classList.remove('active');
  $formEditarMedico.reset();
  $modalConfirmacionEditar.classList.add('active');
  $mensajeConfirmar.innerHTML = `<p>No se puede editar el documento</p>`;
});

$formMedicoBusqueda.addEventListener('submit', (e) => {
  e.preventDefault();
  fetchMedicos(url, $formMedicoBusqueda.buscar_medico.value);
});

document.addEventListener('keyup', (e) => {
  if (e.key === 'Escape') {
    $modalConfirmacionEditar.classList.remove('active');
    $modalEditar.classList.remove('active');
    $modalVerMedico.classList.remove('active');
    $modalEliminar.classList.remove('active');
  }
});
