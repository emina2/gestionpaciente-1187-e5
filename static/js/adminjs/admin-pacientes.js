let $formPacienteBusqueda = document.getElementById('paciente-busqueda-form'),
  $mensaje = document.querySelector('.informacion-usuario-encontrados'),
  $tbody = document.querySelector('.tbody-usuarios'),
  $template = document.querySelector('.template-tabla').content,
  $fragment = document.createDocumentFragment(),
  $loader = document.querySelector('.loader'),
  $modalVerPaciente = document.querySelector('.container-ver-paciente'),
  $mensajeModalVer = document.querySelector('.mensaje-modal-ver');

const url = location.origin;

//recibimos un array con todos los pacientes encontraados
//Mostramos los datos en la tabla
const mostrarDatosTabla = (pacientes) => {
  $tbody.innerHTML = '';
  $mensaje.innerHTML = '';

  pacientes.forEach((paciente) => {
    let $clone = document.importNode($template, true);
    $clone.querySelector('.td_nombres').textContent = paciente.nombreUsuario;
    $clone.querySelector('.td_apellidos').textContent = paciente.apellidoUsuario;
    $clone.querySelector('.td_correo').textContent = paciente.correoElectronico;
    $clone.querySelector('.td_tipo-documento').textContent = paciente.tipoDocumento;
    $clone.querySelector('.td_documento').textContent = paciente.numDocumento;
    $clone.querySelector('.btn-ver').dataset.nombres = paciente.nombreUsuario;
    $clone.querySelector('.btn-ver').dataset.apellidos = paciente.apellidoUsuario;
    $clone.querySelector('.btn-ver').dataset.tipodoc = paciente.tipoDocumento;
    $clone.querySelector('.btn-ver').dataset.documento = paciente.numDocumento;
    $clone.querySelector('.btn-ver').dataset.email = paciente.correoElectronico;
    $clone.querySelector('.btn-ver').dataset.genero = paciente.genero;
    $clone.querySelector('.btn-ver').dataset.estado = paciente.estado;
    $clone.querySelector('.btn-ver').dataset.tipousuario = paciente.nombreRol;
    $clone.querySelector('.btn-editar').dataset.idpaciente = paciente.numDocumento;
    $clone.querySelector('.btn-estado').dataset.idpaciente = paciente.numDocumento;
    if (paciente.estado === 1) {
      $clone.querySelector('.btn-estado').textContent = '❌ Borrar';
    } else {
      $clone.querySelector('.btn-estado').textContent = '🔄 Activar';
    }
    $clone.querySelector('.btn-estado').dataset.estado = paciente.estado;
    $clone.querySelector('.btn-historial').dataset.idpaciente = paciente.numDocumento;

    $fragment.append($clone);
  });
  $tbody.append($fragment);
};

/**Mostrar los datos de un solo paciente
 * se despliega como modal
 */
const mostrarDatosPaciente = (datos) => {
  $modalVerPaciente.classList.add('active');
  $mensajeModalVer.innerHTML = '';
  $mensajeModalVer.innerHTML = `
  <h4>Nombres: ${datos.nombres}</h4>
  <h4>Apellidos: ${datos.apellidos}</h4>
  <h4>Tipo Documento: ${datos.tipo_doc}</h4>
  <h4>Documento: ${datos.documento}</h4>
  <h4>Email: ${datos.email}</h4>
  <h4>Genero: ${datos.genero === 'F' ? 'Femenino' : 'Masculino'}</h4>
  <h4>Tipo usuario: ${datos.tipousuario}</h4>
  <h4>Estado: ${datos.estado === '1' ? 'Activo' : 'Inactivo'}</h4>
  `;
};

//Consumimos la api
//la variable url es la ip de la pagina, la variable dato es lo que buscará
const fetchPacientes = async (url, dato) => {
  try {
    $loader.classList.add('active');
    let res = await fetch(`${url}/admin/pacientes-resultados/${dato}`);

    if (!res.ok) {
      throw new Error('No se encontró la pagina');
    }
    let data = await res.json();

    //la respuesta trae una propiedad llamada pacientes_encontrados
    if (data.pacientes_encontrados === 0) {
      $mensaje.innerHTML = '';
      $tbody.innerHTML = '';
      $mensaje.innerHTML = `<h3 class="informacion-usuario-mensaje">${data.mensaje}</h3 >`;
    } else {
      mostrarDatosTabla(data.resultado);
    }
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

/**Evento para hacer la consulta para buscar pacientes */
$formPacienteBusqueda.addEventListener('submit', (e) => {
  e.preventDefault();
  fetchPacientes(url, $formPacienteBusqueda.buscar_paciente.value);
});

/****************************EDITAR PACIENTE****************************** */
let $formEditarPaciente = document.querySelector('.form-editar-paciente'),
  $modalEditar = document.querySelector('.container-modal-editar'),
  $modalConfirmacionEditar = document.querySelector('.container-modal-confirmar-editar'),
  $mensajeConfirmar = document.querySelector('.mensaje-modal-confirmar');

const validarInputs = (input, dato) => {
  if (input.value.trim() === '') {
    console.log(`${dato} es requerido`);
    return false;
  }
};

const llenarFormularioEditar = (paciente) => {
  $formEditarPaciente.editar_tipo_documento.value = paciente.tipo_documento;
  $formEditarPaciente.editar_documento.value = paciente.documento;
  $formEditarPaciente.editar_nombres.value = paciente.nombres;
  $formEditarPaciente.editar_apellidos.value = paciente.apellidos;
  $formEditarPaciente.editar_genero.value = paciente.genero;
  $formEditarPaciente.editar_correo.value = paciente.correo;
  $formEditarPaciente.editar_rol.value = paciente.rol;
};

const getPacienteFetch = async (url, id) => {
  try {
    $loader.classList.add('active');
    let res = await fetch(`${url}/admin/paciente-consultar/${id}`);

    if (!res.ok) {
      throw new Error('No hay respuesta');
    }

    let data = await res.json();
    //console.log(data);
    llenarFormularioEditar(data.informacion);
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

const editarPacienteFetch = async (url, form) => {
  try {
    $loader.classList.add('active');
    let $form = new FormData(form);
    let options = {
      method: 'POST',
      body: $form,
    };
    let res = await fetch(`${url}/admin/paciente-editar`, options);

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    let data = await res.json();

    $modalEditar.classList.remove('active');
    $modalConfirmacionEditar.classList.add('active');
    $mensajeConfirmar.innerHTML = `<p>${data.mensaje}</p>`;
  } catch (error) {
    console.log(error);
  } finally {
    $loader.classList.remove('active');
  }
};

/****************************ELIMINAR PACIENTE****************************** */
let $modalEliminar = document.querySelector('.container-modal-confirmar-eliminar'),
  $mensajeModalEliminar = document.querySelector('.mensaje-modal-confirmar-eliminar'),
  $btnEliminar = document.querySelector('.btn-eliminar-submit');

const mostrarModalEliminar = (id, estado) => {
  $modalEliminar.classList.add('active');
  $btnEliminar.dataset.idpaciente = id;
  if (estado === '1') {
    $mensajeModalEliminar.textContent = '¿Desea deshabilitar al paciente?';
  } else {
    $mensajeModalEliminar.textContent = '¿Desea habilitar al paciente?';
  }
};

const eliminarPacienteFetch = async (url, id) => {
  try {
    let res = await fetch(`${url}/admin/pacientes/eliminar/${id}`);
    //console.log(res);

    if (!res.ok) throw { mensaje: res.statusText, status: res.status };

    let json = await res.json();
    //console.log(json);
  } catch (error) {
    console.log(`${error.status}: ${error.mensaje}`);
  }
};

/****************************VER HISTORIAL PACIENTE****************************** */
let $modalHistorial = document.querySelector('.container-modal-historial-paciente'),
  $mensajeHistorial = document.querySelector('.mensaje-modal-historial-paciente'),
  $contenedorCitas = document.querySelector('.citas-historial'),
  $templateCita = document.getElementById('template-cita-historial').content;

const mostrarHistorial = (datos) => {
  $modalHistorial.classList.add('active');
  $contenedorCitas.innerHTML = '';
  let $titulo = document.createElement('h3');

  $titulo.textContent = datos.mensaje;
  $titulo.classList.add('citas-historial-titulo');

  datos.informacion.forEach((cita) => {
    let $clone = $templateCita.cloneNode(true);
    $clone.querySelector('.cita-id').innerHTML = `IdCita: <strong>${cita.id_cita}</strong>`;
    $clone.querySelector('.cita-fecha').innerHTML = `Fecha cita: <strong>${cita.fecha}</strong>`;
    $clone.querySelector(
      '.cita-id-medico'
    ).innerHTML = `Id Medico: <strong>${cita.doc_medico}</strong>`;
    $clone.querySelector(
      '.cita-nombre-medico'
    ).innerHTML = `Nombre Medico: <strong>${cita.nombre_medico}</strong>`;
    $clone.querySelector(
      '.cita-apellido-medico'
    ).innerHTML = `Apellido Medico: <strong>${cita.apellido_medico}</strong>`;
    $clone.querySelector(
      '.cita-id-paciente'
    ).innerHTML = `Id Paciente: <strong>${cita.doc_paciente}</strong>`;
    $clone.querySelector(
      '.cita-nombre-paciente'
    ).innerHTML = `Nombre Paciente: <strong>${cita.nombre_paciente}</strong>`;
    $clone.querySelector(
      '.cita-apellido-paciente'
    ).innerHTML = `Apellido Paciente: <strong>${cita.apellido_paciente}</strong>`;
    $clone.querySelector('.cita-comentario-container').textContent = `${cita.comentario}`;

    $fragment.append($clone);
  });

  $contenedorCitas.append($titulo);
  $contenedorCitas.append($fragment);
};

const historialPacienteFetch = async (url, id) => {
  try {
    let res = await fetch(`${url}/admin/paciente-historial/${id}`);
    //console.log(res);

    if (!res.ok) throw { mensaje: res.statusText, status: res.status };

    let json = await res.json();
    mostrarHistorial(json);
  } catch (error) {
    console.log(`${error.status}: ${error.mensaje}`);
  }
};

/*************************************EVENTOS************************************** */

document.addEventListener('click', (e) => {
  if (e.target.matches('.btn-ver')) {
    let datos = {
      nombres: e.target.dataset.nombres,
      apellidos: e.target.dataset.apellidos,
      tipo_doc: e.target.dataset.tipodoc,
      documento: e.target.dataset.documento,
      email: e.target.dataset.email,
      genero: e.target.dataset.genero,
      estado: e.target.dataset.estado,
      tipousuario: e.target.dataset.tipousuario,
    };
    mostrarDatosPaciente(datos);
  }

  if (e.target.matches('.btn-cerrar-ver') || e.target.matches('.btn-cerrar-ver *')) {
    $modalVerPaciente.classList.remove('active');
  }
  if (e.target.matches('.btn-editar')) {
    e.stopPropagation();
    $modalEditar.classList.add('active');
    let id = e.target.dataset.idpaciente;
    getPacienteFetch(url, id);
  }

  if (e.target.matches('.btn-editar-cancelar')) {
    $formEditarPaciente.reset();
    $modalEditar.classList.remove('active');
  }

  if (e.target.matches('.btn_editar_submit')) {
    editarPacienteFetch(url, $formEditarPaciente);
  }

  if (e.target.matches('.btn-cerrar-confirmar')) {
    $modalConfirmacionEditar.classList.remove('active');
    location.reload();
  }

  if (e.target.matches('.btn-estado')) {
    let id = e.target.dataset.idpaciente;
    let estado = e.target.dataset.estado;
    mostrarModalEliminar(id, estado);
  }

  if (e.target === $btnEliminar) {
    let id = e.target.dataset.idpaciente;
    eliminarPacienteFetch(url, id);
    location.reload();
  }

  if (e.target.matches('.btn-eliminar-cancelar')) {
    $modalEliminar.classList.remove('active');
    $btnEliminar.dataset.idpaciente = '';
  }

  if (e.target.matches('.btn-historial')) {
    let id = e.target.dataset.idpaciente;
    historialPacienteFetch(url, id);
  }
  if (e.target.matches('.btn-cerrar-historial-paciente')) {
    $modalHistorial.classList.remove('active');
  }
});

$formEditarPaciente.addEventListener('submit', (e) => {});

$formEditarPaciente.editar_documento.addEventListener('change', (e) => {
  $modalEditar.classList.remove('active');
  $formEditarPaciente.reset();
  $modalConfirmacionEditar.classList.add('active');
  $mensajeConfirmar.innerHTML = `<p>No se puede editar el documento</p>`;
});

document.addEventListener('keyup', (e) => {
  if (e.key === 'Escape') {
    $modalConfirmacionEditar.classList.remove('active');
    $modalEditar.classList.remove('active');
    $modalVerPaciente.classList.remove('active');
    $modalEliminar.classList.remove('active');
    $modalHistorial.classList.remove('active');
  }
});
