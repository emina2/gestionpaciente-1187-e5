console.log('conectado');

const $formCrearMedico = document.getElementById('crear-medico-form'),
  $selectEspecialidad = document.getElementById('especialidad_medico'),
  $fragment = document.createDocumentFragment(),
  $modalCrearMedico = document.querySelector('.container-modal-crear-medico'),
  $mensajeModalCrear = document.querySelector('.mensaje-modal-crear-medico');

const url = location.origin;

const mostrarEspecialidades = (especialidades) => {
  especialidades.forEach((especialidad) => {
    let $opt = document.createElement('option');
    $opt.value = especialidad.id;
    $opt.text = especialidad.nombre;
    $fragment.append($opt);
  });
  $selectEspecialidad.append($fragment);
};

const obtenerEspecialidades = async (url) => {
  try {
    let resp = await fetch(`${url}/admin/medicos/especialidades`);

    if (!resp.ok) throw { mensaje: resp.statusText, status: resp.status };

    let json = await resp.json();

    mostrarEspecialidades(json);
  } catch (error) {
    console.log(`${error.status}:${error.mensaje}`);
  }
};

const validarMail = (mail = '') => {
  if (!mail) return false;
  /* /[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/i */
  let expMail = /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,15})+$/;
  return expMail.test(mail);
};

const mostrarModal = (datos) => {
  $modalCrearMedico.classList.add('active');
  $mensajeModalCrear.innerHTML = `<h3>${datos.mensaje}</h3>`;
  if (datos.estado) {
    $formCrearMedico.reset();
  }
};

const crearMedico = async (url) => {
  let form = new FormData($formCrearMedico);
  try {
    let options = {
      method: 'POST',
      body: form,
    };
    let resp = await fetch(`${url}/admin/medicos/agregar`, options);

    if (!resp.ok) throw { mensaje: resp.statusText, status: resp.status };

    let json = await resp.json();
    console.log(json);
    mostrarModal(json);
  } catch (error) {
    console.log(`${error.status}:${error.mensaje}`);
  }
};

document.addEventListener('DOMContentLoaded', (e) => {
  obtenerEspecialidades(url);
});

document.addEventListener('click', (e) => {
  if (e.target.matches('.btn-crear-medico')) {
    if (!validarMail($formCrearMedico.email_medico.value)) {
      mostrarModal({ mensaje: 'Email no valido', estado: false });
      return false;
    }
    crearMedico(url);
  }
  if (e.target.matches('.btn-aceptar')) {
    $modalCrearMedico.classList.remove('active');
  }
});
