console.log('conectado');

let $modalComentario = document.querySelector('.container-modal-comentario-cita'),
  $mensajeComentario = document.querySelector('.mensaje-modal-comentario');

const url = location.origin;

/*************************MOSTRAR COMENTARIO****************************** */
const mostrarComentario = (info) => {
  $modalComentario.classList.add('active');
  $mensajeComentario.innerHTML = '';
  $mensajeComentario.innerHTML = `<h3>Cita Finalizada</h3>
  <div>Id Cita: ${info.id_cita}</div>
  <div><span><strong>Comentario:</strong> </span>
    <div>${info.comentario}</div>
  </div>
  `;
};

const obtenerComentario = async (url, ruta) => {
  try {
    let res = await fetch(`${url}${ruta}`);
    console.log(res);
    if (!res.ok) throw { mensaje: res.statusText, status: res.status };

    let json = await res.json();
    console.log(json);
    mostrarComentario(json.informacion);
  } catch (error) {
    console.log(`${error.status}: ${error.mensaje}`);
  }
};

/**********************CALIFICAR CITA********************* */
const $modalCalificar = document.querySelector('.container-modal-calificar-cita'),
  $inputCalificar = document.getElementById('calificacion-range'),
  $btnEnviarCalificar = document.querySelector('.btn-cita-calificar-enviar'),
  $modalExitoCal = document.querySelector('.container-modal-calificacion-exito-cita'),
  $mensajeExito = document.querySelector('.mensaje-modal-calificacion-exito'),
  $calificacionShow = document.querySelector('.calificacion-value span');

let calificaciones = {
  1: '⭐',
  2: '⭐⭐',
  3: '⭐⭐⭐',
  4: '⭐⭐⭐⭐',
  5: '⭐⭐⭐⭐⭐',
};

const calificarCita = async (url, id, cal) => {
  try {
    let res = await fetch(`${url}/paciente/calificar-cita/${id}/${cal}`);
    console.log(res);
    if (!res.ok) throw { mensaje: res.statusText, status: res.status };
    let json = await res.json();
    console.log(json);
    $modalExitoCal.classList.add('active');
    $mensajeExito.textContent = json.mensaje;
  } catch (error) {
    console.log(`${error.status}: ${error.mensaje}`);
  }
};

/***************************EVENTOS************************** */
document.addEventListener('input', (e) => {
  if (e.target === $inputCalificar) {
    let value = $inputCalificar.value;
    $btnEnviarCalificar.dataset.califiacion = value;
    $calificacionShow.classList.add('show');
    $calificacionShow.textContent = ``;
    $calificacionShow.textContent = `${calificaciones[value]}`;
    $calificacionShow.style.left = value * 20 + '%';
  }
});

$inputCalificar.addEventListener('blur', (e) => {
  console.log('salio');
  $calificacionShow.classList.remove('show');
});

document.addEventListener('click', (e) => {
  if (e.target.matches('.btn-cita-comentario')) {
    e.preventDefault();
    let rutaComentario = e.target.getAttribute('href');
    //console.log(rutaComentario);
    obtenerComentario(url, rutaComentario);
  }
  if (e.target.matches('.btn-cerrar-comentario')) {
    $modalComentario.classList.remove('active');
  }

  //Click boton calificar
  if (e.target.matches('.btn-cita-calificar')) {
    let idCita = e.target.getAttribute('data-idcita');
    $btnEnviarCalificar.dataset.idcita = idCita;
    $btnEnviarCalificar.dataset.califiacion = $inputCalificar.value;
    $modalCalificar.classList.add('active');
  }
  if (e.target.matches('.btn-cerrar-calificar')) {
    $modalCalificar.classList.remove('active');
    $btnEnviarCalificar.dataset.idcita = '';
    $btnEnviarCalificar.dataset.califiacion = '';
    $inputCalificar.value = '3';
  }

  //Click boton Enviar Calificacion
  if (e.target === $btnEnviarCalificar) {
    $modalCalificar.classList.remove('active');
    let idCita = $btnEnviarCalificar.dataset.idcita;
    let cal = $btnEnviarCalificar.dataset.califiacion;
    calificarCita(url, idCita, cal);
  }

  if (e.target.matches('.btn-cerrar-calificacion-exito')) {
    $modalExitoCal.classList.remove('active');
    $mensajeExito.innerHTML = '';
    location.reload();
  }
});
