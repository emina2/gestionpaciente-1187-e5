console.log('conectado');

let $modalCancelar = document.querySelector('.container-modal-cancelar-cita'),
  $mensajeCancelar = document.querySelector('.mensaje-cancelar-cita'),
  $btnAceptarCancelar = document.querySelector('.btn-cancelar-cita-aceptar');

let url = `${location.origin}/paciente`;

const fechtCancelarCita = async (url, fecha, medico, id) => {
  try {
    let res = await fetch(`${url}/mis-citas/cancelar-cita/${fecha}/${medico}/${id}`);

    if (!res.ok) {
      throw new Error('No se encontró el recurso');
    }

    let data = await res.json();

    if (data.cancelada) {
      window.location.reload();
    } else {
      console.log(data.mensaje);
    }
  } catch (error) {
    console.log(error);
  }
};

document.addEventListener('click', (e) => {
  if (e.target.matches('.btn-cancelar-cita')) {
    $modalCancelar.classList.add('active');
    $btnAceptarCancelar.dataset.id = e.target.dataset.id;
    $btnAceptarCancelar.dataset.idMedico = e.target.dataset.idmedico;
    $btnAceptarCancelar.dataset.fecha = e.target.dataset.fecha;
  }
  if (e.target.matches('.btn-cancelar-cita-cancelar')) {
    $modalCancelar.classList.remove('active');
  }
  if (e.target === $btnAceptarCancelar) {
    let id = $btnAceptarCancelar.dataset.id;
    let medico = $btnAceptarCancelar.dataset.idMedico;
    let fecha = $btnAceptarCancelar.dataset.fecha;
    fechtCancelarCita(url, fecha, medico, id);
  }
});
