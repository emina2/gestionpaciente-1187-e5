let $formCrearCita = document.getElementById('nuevaCitaForm'),
  $id = document.getElementById('id_paciente'),
  $fecha = document.getElementById('fecha_cita'),
  $ciudad = document.getElementById('ciudad'),
  $especialidad = document.getElementById('especialidad_cita'),
  $medico = document.getElementById('medico_cita'),
  $btnCrearCita = document.getElementById('btn_crear_cita'),
  $modal = document.querySelector('.modal-container'),
  $modalMensaje = document.querySelector('.modal-mensaje'),
  $btnCerrar = document.querySelector('.modal-btnCerrar'),
  $confiCitaMsj = document.querySelector('.modal-confirmar-cita-mensaje'),
  $modalConifCita = document.querySelector('.form-modal-container'),
  $modalConfMensaje = document.querySelector('.modal-confirmar-cita-mensaje'),
  $btnCancelat = document.querySelector('.btn-cancelar'),
  $formConfCita = document.getElementById('form_conf_cita');

const url = location.origin;

const validarFechaCita = (fechaCita) => {
  //Validamos si el campo fecha esta vacio
  if ($fecha.value.trim() === '') {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<p>Seleccione una fecha</p>`;
    return false;
  }
  //obtenemos la fecha actual en milisegundos
  fechaActual = Date.now();
  //obtenemos la fecha de la cita
  fechaAValidar = new Date(fechaCita);

  //obtenemos el dia de la semana(0-6... 0 = domingo, 1 = lunes, etc)
  diaValidar = fechaAValidar.getDay();

  //Comparamos la fecha actual y la fecha a validar
  //Usamos getTime() para pasar la fecha a validar a milisegundos
  if (fechaActual >= fechaAValidar.getTime()) {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<h3>Cita no disponible</h3>
    <p>La fecha no puede ser igual o anterior al dia actual</p>`;
    return false;
  }
  //Validamos si el dia de semana de la cita es domingo o sabado
  if (diaValidar === 0 || diaValidar === 6) {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<h3>Cita no disponible</h3>
    <p>No se atienden los fines de semana</p>`;
    return false;
  }
  return true;
};

const validarCiudad = (input) => {
  if (input.value.trim() === '') {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<p>La ciudad esta vacia</p>`;
    return false;
  }
  if (input.value.trim() !== 'Barranquilla') {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<p>La ciudad debe ser Barranquilla</p>`;
    return false;
  }
  return true;
};

const validarEspecialidad = (input) => {
  if (input.value.trim() === '') {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<p>La especialidad esta vacia</p>`;
    return false;
  }
  if (
    input.value.trim() !== 'Medicina General' &&
    input.value.trim() !== 'Odontologia' &&
    input.value.trim() !== 'Oftalmologia'
  ) {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<p>La especialidad debe ser: 
    <ul>
      <li>medicina-general</li>
      <li>odontologia</li>
      <li>oftalmologia</li>
    </ul>
    </p>`;
    return false;
  }
  return true;
};

const validarMedico = (input) => {
  if (input.value.trim() === '' || input.value.trim() === '0') {
    $modal.classList.add('active');
    $modalMensaje.innerHTML = `<p>No se seleccionó ningun medico</p>`;
    return false;
  }
  return true;
};

const mostrarConfirmacion = (datos) => {
  $modalConifCita.classList.add('active');
  $formConfCita.confirmar_fecha_cita.value = datos.fecha;
  $formConfCita.confirmar_ciudad.value = 'Barranquilla';
  $formConfCita.confirmar_especialidad_cita.value = datos.especialidad;
  $formConfCita.confirmar_medico_cita.value = datos.medico;
  $formConfCita.confirmar_turno_cita.value = datos.turno;
  // $modalConfMensaje.innerHTML = `
  // <h3>${datos.titulo}</h3>
  // <div class="modal-mensaje-confirmar">
  //   <span>Fecha: ${datos.fecha}</span>
  //   <span>Turno: ${datos.turno}</span>
  // </div>
  // <div class="modal-mensaje-confirmar">
  //   <span>Especialidad: ${datos.especialidad}</span>
  //   <span>Medico: ${datos.medico}</span>
  // </div>

  // `;
};

const mostrarDisponibilidad = (mensaje) => {
  $modal.classList.add('active');
  $modalMensaje.innerHTML = `<p>${mensaje}</p>`;
};

const fetchCita = async (url, fecha, espec, med, id) => {
  try {
    /**Cambiamos el dato que enviamos en el input de especialidad al dato requerido */
    if (espec === 'Medicina General') {
      espec = 1;
    } else if (espec === 'Odontologia') {
      espec = 2;
    } else if (espec === 'Oftalmologia') {
      espec = 3;
    }

    let res = await fetch(`${url}/paciente/validar-cita/${fecha}/${espec}/${med}/${id}`);
    if (!res.ok) {
      throw new Error('Pagina no encontrada');
    }
    let data = await res.json();
    console.log(data);

    if (data.disponible) {
      let datos_cita = {
        titulo: data.mensaje,
        fecha: data.informacion.fecha,
        especialidad: data.informacion.especialidad,
        turno: data.informacion.turno,
        medico: data.informacion.medico,
      };
      console.log(datos_cita);
      mostrarConfirmacion(datos_cita);
    } else {
      mostrarDisponibilidad(data.mensaje);
    }
  } catch (error) {
    console.log(error);
  }
};

document.addEventListener('click', (e) => {
  if (e.target === $btnCerrar) {
    //le removemos la clase active al modal para que se cierre
    $modal.classList.remove('active');
    $modalMensaje.innerHTML = '';
  }

  if (e.target === $btnCancelat) {
    $modalConifCita.classList.remove('active');
  }
});

/**Formulario para crear cita
 * Valida la fecha
 * valida la ciudad
 * valida la especialidad
 * valida el medico
 */
$formCrearCita.addEventListener('submit', (e) => {
  e.preventDefault();
  if (!validarFechaCita($fecha.value)) return false;
  if (!validarCiudad($ciudad)) return false;
  if (!validarEspecialidad($especialidad)) return false;
  if (!validarMedico($medico)) return false;
  fetchCita(url, $fecha.value, $especialidad.value, $medico.value, $id.value);
});

$formConfCita.addEventListener('submit', (e) => {});
