'''
Librerias a instalar
* pip install flask-wtf
* pip install WTForms
* pip install email_validator    para validar el email
'''

from wtforms import validators
from flask_wtf import FlaskForm
from wtforms import Form
from wtforms import StringField, PasswordField, SubmitField, SelectField, TextAreaField
from wtforms.fields.html5 import EmailField, DateField, IntegerField
from wtforms.validators import DataRequired
import email_validator
import sqlite3

'''
Formulario para Login
'''


class Login_form(FlaskForm):
    '''
    Creamos los inputs que importaremos en el app
    emailuser, sera el id y el name del input
    '''
    emailuser = EmailField(
        'Correo: ',
        validators=[
            validators.email(message='El correo no tiene un formato válido'),
            DataRequired(message='Correo requerido')
        ],
        render_kw={"placeholder": "Ingresar Correo"})

    password_login = PasswordField(
        'Password: ',
        validators=[DataRequired(message='Password requerido')],
        render_kw={"placeholder": "Ingresar Password"})

    submitLogin = SubmitField('Iniciar Sesión')


'''
Formulario para Registro de usuario en plataforma
'''


class Registro_form(FlaskForm):
    nombres_usuario = StringField(
        'Nombres: ',
        validators=[DataRequired(message='El nombre es requerido')],
        render_kw={"placeholder": "Ingresar Nombres"})

    apellidos_usuario = StringField(
        'Apellidos: ',
        validators=[DataRequired(message='Los apellidos son requeridos')],
        render_kw={"placeholder": "Ingresar Apellidos"})

    tipo_documento = SelectField(
        'Tipo Documento',
        choices=[('', '-tipo documento-'), ('cc', 'C.C'),
                 ('ti', 'T.I'), ('ce', 'C.E')],
        validators=[DataRequired(message='El tipo de documento es requerido')],
        validate_choice=True)

    documento_usuario = IntegerField(
        'Documento: ',
        validators=[DataRequired(message='El documento es requerido')],
        render_kw={"placeholder": "Ingresar Documento"})

    genero_usuario = SelectField(
        'Genero',
        choices=[('', '-genero-'), ('M', 'Masculino'), ('F', 'Femenino')],
        validators=[DataRequired(message='El genero es requerido')],
        validate_choice=True)

    email_usuario = EmailField(
        'Correo: ',
        validators=[
            validators.email(message='Correo no valido'),
            DataRequired(message='Correo requerido')
        ],
        render_kw={"placeholder": "Ingresar Correo"})

    password_usuario = PasswordField(
        'Password: ',
        validators=[DataRequired(message='Password requerido')],
        render_kw={"placeholder": "Ingresar Password"})

    submitLogin = SubmitField('Registrarse')


'''
Formulario para que el paciente edite su cita
'''


class EditarCitaForm(FlaskForm):
    paciente_id = StringField("Documento",
                              validators=[DataRequired(
                                  "El número de documento del paciente es requerido")],
                              render_kw=({"readonly": True}))
    especialidad_id_select = SelectField("Especialidad",
                                         validators=[DataRequired(
                                             "Debe seleccionar una especialidad")],
                                         choices=[('', '-Seleccione-'), ('1', 'Medicina General'),
                                                  ('2', 'Odontologia'), ('3', 'Oftalmologia')],
                                         validate_choice=True,
                                         render_kw=({"disabled": True}))
    medico_id_select = SelectField("Médico",
                                   validators=[DataRequired(
                                       "Debe seleccionar un médico")],
                                   choices=[('', '-Seleccione-')],
                                   validate_choice=True)
    fecha_cita = DateField("Fecha cita",
                           format='%d/%m/%Y',
                           validators=[DataRequired("La fecha es requerida")])

    btnEditar = SubmitField("Editar")
    btnCancelar = SubmitField("Cancelar", render_kw=({"formnovalidate": True, "class":"btn-cancelar"}))


'''
Formulario para Crear nueva cita
'''


class Nueva_cita_form(FlaskForm):

    id_paciente = StringField('Documento Paciente:', validators=[DataRequired(
        message='Documento requerido')],
        render_kw={'readonly': True})

    fecha_cita = DateField('Seleccionar Fecha: ', format='%m/%d/%Y',
                           validators=[DataRequired(message='Fecha requerida')])

    ciudad = StringField('Ciudad:', validators=[DataRequired(
        message='Fecha requerida')],
        render_kw={'readonly': True})

    especialidad_cita = StringField('Especialidad:', validators=[DataRequired(
        message='Especialidad requerida')],
        render_kw={'readonly': True})

    turno_cita = StringField('', validators=[DataRequired(
        message='Turno requerido')],
        render_kw={'readonly': True})

    medico_cita = SelectField(
        'Seleccionar medico: ',
        choices=[('', '-medico-'), ('1', 'Juand'), ('2', 'Lucia')],
        validators=[DataRequired(message='El medico es requerido')],
        validate_choice=True)

    crear_cita = SubmitField('Crear Cita')


'''
Formulario para Confirmar cita
'''


class Confirmar_cita_form(FlaskForm):
    confirmar_id_paciente = StringField('Documento Paciente:', validators=[DataRequired(
        message='Documento requerido')],
        render_kw={'readonly': True})

    confirmar_fecha_cita = StringField('Fecha: ', validators=[DataRequired(
        message='Fecha requerida')],
        render_kw={'readonly': True})

    confirmar_ciudad = StringField('Ciudad:', validators=[DataRequired(
        message='Fecha requerida')],
        render_kw={'readonly': True})

    confirmar_especialidad_cita = StringField('Especialidad:', validators=[DataRequired(
        message='Especialidad requerida')],
        render_kw={'readonly': True})

    confirmar_medico_cita = StringField(
        'Medico: ',
        validators=[DataRequired(message='El medico es requerido')],
        render_kw={'readonly': True})

    confirmar_turno_cita = StringField('Turno:', validators=[DataRequired(
        message='Turno requerido')],
        render_kw={'readonly': True})

    confirmar_submit_cita = SubmitField('Confirmar')


'''
Formulario para Busqueda de paciente
'''


class Busqueda_Paciente(FlaskForm):
    buscar_paciente = StringField('Paciente:', validators=[DataRequired(
        message='Datos requeridos')],
        render_kw={"placeholder": "Ingresar Dato del paciente"})
    bucar_paciente_submit = SubmitField('Buscar paciente')


'''
Formulario para Atender cita del medico
'''


class Atender_Cita(FlaskForm):

    comentarios_cita = TextAreaField('Comentarios:', validators=[DataRequired(
        message='Comentarios requerido')])

    finalizar_cita = SubmitField('Finalizar Cita')


'''
Formulario para busqueda de medico
'''


class Busqueda_Medico(FlaskForm):
    buscar_medico = StringField('Medico:', validators=[DataRequired(
        message='Datos requeridos')],
        render_kw={"placeholder": "Ingresar Dato del medico"})
    bucar_medico_submit = SubmitField('Buscar medico')


'''
Formulario para Crear paciente
'''


class Crear_Paciente_Form(FlaskForm):
    nombres_paciente = StringField(
        'Nombres: ',
        validators=[DataRequired(message='El nombre es requerido')],
        render_kw={"placeholder": "Ingresar Nombres"})

    apellidos_paciente = StringField(
        'Apellidos: ',
        validators=[DataRequired(message='Los apellidos son requeridos')],
        render_kw={"placeholder": "Ingresar Apellidos"})

    tipo_documento_paciente = SelectField(
        'Tipo Documento',
        choices=[('', '-tipo documento-'), ('cc', 'C.C'),
                 ('ti', 'T.I'), ('ce', 'C.E')],
        validators=[DataRequired(message='El tipo de documento es requerido')],
        validate_choice=True)

    documento_paciente = IntegerField(
        'Documento: ',
        validators=[DataRequired(message='El documento es requerido')],
        render_kw={"placeholder": "Ingresar Documento"})

    genero_paciente = SelectField(
        'Genero',
        choices=[('', '-genero-'), ('M', 'Masculino'), ('F', 'Femenino')],
        validators=[DataRequired(message='El genero es requerido')],
        validate_choice=True)

    email_paciente = EmailField(
        'Correo: ',
        validators=[
            validators.email(message='Correo no valido'),
            DataRequired(message='Correo requerido')
        ],
        render_kw={"placeholder": "Ingresar Correo"})

    password_paciente = PasswordField(
        'Password: ',
        validators=[DataRequired(message='Password requerido')],
        render_kw={"placeholder": "Ingresar Password"})

    submit_crear_paciente = SubmitField('Crear Paciente')


class Crear_Medico_Form(FlaskForm):
    nombres_medico = StringField(
        'Nombres: ',
        validators=[DataRequired(message='El nombre es requerido')],
        render_kw={"placeholder": "Ingresar Nombres"})

    apellidos_medico = StringField(
        'Apellidos: ',
        validators=[DataRequired(message='Los apellidos son requeridos')],
        render_kw={"placeholder": "Ingresar Apellidos"})

    tipo_documento_medico = SelectField(
        'Tipo Documento',
        choices=[('', '-tipo documento-'), ('cc', 'C.C'),
                 ('ti', 'T.I'), ('ce', 'C.E')],
        validators=[DataRequired(message='El tipo de documento es requerido')],
        validate_choice=True)

    documento_medico = IntegerField(
        'Documento: ',
        validators=[DataRequired(message='El documento es requerido')],
        render_kw={"placeholder": "Ingresar Documento"})


    email_medico = EmailField(
        'Correo: ',
        validators=[
            validators.email(message='Correo no valido'),
            DataRequired(message='Correo requerido')
        ],
        render_kw={"placeholder": "Ingresar Correo"})

    password_medico = PasswordField(
        'Password: ',
        validators=[DataRequired(message='Password requerido')],
        render_kw={"placeholder": "Ingresar Password"})

    submit_crear_medico = SubmitField('Crear Paciente')