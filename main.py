from flask import Flask, render_template, redirect, request, jsonify, session, flash
from views.paciente import paciente
from views.doctor import doctor
from views.admin import admin
import sqlite3
import os
import hashlib  # encriptar
import random  # generar id cita de manera aleatoria
from forms.forms import Busqueda_Paciente, Confirmar_cita_form, Login_form, Nueva_cita_form, Registro_form
from flask.helpers import url_for
from werkzeug.utils import escape
from flask_wtf.csrf import CsrfProtect

app = Flask(__name__)
CsrfProtect(app)
# registramos los blueprint, es decir, los archivos que manejan la logica para admin, doctor y paciente
app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(doctor, url_prefix='/doctor')
app.register_blueprint(paciente, url_prefix='/paciente')

app.secret_key = os.urandom(32)
#Utilizar cuando se suba a la nube
#homeServer = "/home/pmonteroa/mysite/"
homeServer = ""
db = f'{homeServer}db/dbProyecto.db'


'''
RUTA INICIAL
'''


@app.route("/", methods=["GET"])
def init():
    title = 'Inicio'
    return render_template('index.html', title=title)


'''
RUTA INICIAR SESION
'''


@app.route("/iniciar-sesion", methods=["GET", "POST"])
def iniciar_sesion():
    title = 'Login'
    login_form = Login_form(request.form)

    if (login_form.validate_on_submit()):
        email = escape(login_form.emailuser.data)
        password = escape(login_form.password_login.data)
        # Encriptar contraseña
        encrp = hashlib.sha256(password.encode())
        pass_enc = encrp.hexdigest()
        # conectamos con la base de datos
        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute('''SELECT nombreUsuario, apellidoUsuario, numDocumento, estado, fk_idRol, correoElectronico 
            FROM usuario 
            where correoElectronico = ? and password = ?''', [
                           email, pass_enc])

            # guardamos el resultado unico en una variable llamado row
            row = cursor.fetchone()
            if row:
                # almacenamos el nombre de las columnas
                columns = [column[0] for column in cursor.description]
                # creamos un diccionario para realizar las busquedas y pasar datos
                usuario = dict(zip(columns, row))

                if usuario['estado'] == 1:

                    session['id_usuario'] = usuario['numDocumento']
                    session['nombre_usuario'] = usuario['nombreUsuario']
                    session['apellido_usuario'] = usuario['apellidoUsuario']
                    session['estado'] = usuario['estado']

                    # print(usuario)
                    # rederigimos a los usuarios segun su rol

                    if usuario['fk_idRol'] == 3:
                        return redirect(url_for('paciente.init', id=usuario['numDocumento']))

                    if usuario['fk_idRol'] == 2:
                        return redirect(url_for('doctor.init', id_medico=usuario['numDocumento']))

                    if usuario['fk_idRol'] == 1:
                        return redirect(url_for('admin.init', id=usuario['numDocumento']))
                
                else:
                    flash('El usuario se encuentra inactivo. Por favor, contacte al administrador del sistema')


            else:
                flash('Usuario o password incorrectos')

    return render_template('/registro-login/login.html',
                           title_login=title,
                           form=login_form)


'''
RUTA REGISTRO PACIENTE
'''
@app.route('/registro-paciente', methods=['GET', 'POST'])
def registro_paciente():
    title = 'Registro'
    class_registro = ''
    registro_form = Registro_form(request.form)

    if registro_form.validate_on_submit():
        nombres = escape(registro_form.nombres_usuario.data)
        apellidos = escape(registro_form.apellidos_usuario.data)
        tipo_doc = escape(registro_form.tipo_documento.data)
        doc = escape(registro_form.documento_usuario.data)
        genero = escape(registro_form.genero_usuario.data)
        email = escape(registro_form.email_usuario.data)
        password = escape(registro_form.password_usuario.data)
        registro_user = True
        encrp = hashlib.sha256(password.encode())
        pass_enc = encrp.hexdigest()

        with sqlite3.connect(db) as con:
            cursor = con.cursor()
            cursor.execute(
                'SELECT correoElectronico, numDocumento FROM usuario where correoElectronico = ? or numDocumento = ?', [email, doc])
            if cursor.fetchone():
                class_registro = 'registro-false'
                flash('Correo o Documento ya registrado. Por favor intenta nuevamente o si ya estas registrado inicia sesión')
            else:
                cursor.execute('INSERT INTO usuario(nombreUsuario,apellidoUsuario,tipoDocumento,numDocumento,correoElectronico,password,estado,fk_idRol) VALUES(?,?,?,?,?,?,?,?)', [
                               nombres, apellidos, tipo_doc, doc, email, pass_enc, 1, 3])
                con.commit()
                cursor.execute(
                    'INSERT INTO paciente (idPaciente,genero) VALUES (?,?)', [doc, genero])
                con.commit()
                class_registro = 'registro-true'
                flash('Usuario registrado exitosamente. A continuación puedes iniciar sesión.')
                return redirect(url_for('.iniciar_sesion'))

    return render_template('/registro-login/registro.html',
                           title=title,
                           form=registro_form,
                           class_registro = class_registro)


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/')


if __name__ == '__main__':
    app.run(debug=True)
